![screenopening](screenshots/screenopening.png =321X288) ![screenbattle](screenshots/screenbattle.png =321X288)

# cthulhus-revenge

A small JRPG in the style of the classic gameboy handheld. Written using SDL2.
Still needs some tidying up and very short.

mainly written just to try out SDL2, originally intended for a game jam with the theme Sea and Space, although did not actually complete before the deadline.

Controls - Arrow Keys to move/change cursor in menus, X - Accept/Interact etc. Z - Back/Bring up menu  

# Build Instructions
Should buld on windows under visual studio 2017, you need to copy the DLLs from SDL_DLL to the exe directory to run

Should also build on Linux under gcc, just need to install SDL2, also uses SDL2_image, SDL2_mixer, SDL2_ttf and their dependencies