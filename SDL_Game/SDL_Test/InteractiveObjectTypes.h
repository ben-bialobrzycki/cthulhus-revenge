#pragma once
#include "Item.h"
enum class EnemyType { None, Pirate, BossPirate, PirateDog };
struct EnemyGroupInfo
{
	int x;             //position on map
	int y;			   //position on map
	EnemyType slot1Enemy;
	EnemyType slot2Enemy;
	EnemyType slot3Enemy;      //enemy groups in battle
	bool Finished;
	SpriteId groupSprite; //Sprite On Map
	GameEventId gameEvent;
};

struct EnemyInfo
{
	char* Name;
	EnemyType enemyType;
	int AttackMin;
	int AttackMax;
	int HitPoints;
	int Defense; //TODO include these in calculations
	int Agility; //TODO include these in calculations
	SpriteId battleSprite;
};

struct ChestInfo
{
	int x;
	int y;
	ItemType itemSlot1;
	ItemType itemSlot2;
	ItemType itemSlot3;
	bool opened;
};
