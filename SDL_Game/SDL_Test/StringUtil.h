#pragma once
#include "Common.h"
#include "InventoryItem.h"
#include "MagicSpell.h"
#include "DialogueCommands.h"

void ItemListToStringList(std::vector<InventoryItem>* itemList, std::vector<std::string>* stringList);
void SpellListToStringList(std::vector<Spell>* AvailableSpells, std::vector<std::string>* stringList);
void BuildDialogueListSpells(std::vector<Spell>* AvailableSpells, std::vector<DialogueOption>* commands);
void BuildDialogueListItems(std::vector<InventoryItem>* inventory, std::vector<DialogueOption>* commands);
