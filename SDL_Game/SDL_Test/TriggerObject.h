#pragma once
#include "globals.h"
#include "Common.h"
#include "Game.h"
#include "globals.h"

enum class TrigObjType{StairsUp, StairsDown};
class TriggerObject
{
public:
	TriggerObject(TrigObjType objType, int x, int y);
	Entity* entity;
	TrigObjType objType;
	void OnCollision(Entity* otherEntity);
	void GoDownStairs(Entity* playerEntity);
	void GoUpStairs(Entity* playerEntity);
	int width, height;
	~TriggerObject();
};



