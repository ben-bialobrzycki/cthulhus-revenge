#include "Physics.h"
#include "Vector2.h"
#include "Entity.h"
#include "TriggerObject.h"
#include "WorldMap.h"

bool Physics::AllowedMovement(Vector2 nextposition, Entity* entity)
{
	std::vector<Entity*> relevantEntities = std::vector<Entity*>();
	GetNearByEntities(entity, &relevantEntities,false);

	BoundingBox entityBox = entity->GetBoundingBox(nextposition);
	for (UINT i = 0; i < relevantEntities.size(); i++)
	{
		Entity* pentity = relevantEntities.at(i);
		if (!pentity->Active)
			continue;
		if (pentity->entityType != EntityWall && pentity->entityType != EntityObject && pentity->entityType != EntityEnemy)
			continue;
		BoundingBox destBox = pentity->GetBoundingBox(pentity->position);
		bool collision = destBox.Intersects(entityBox);
		if (!collision)
			continue;

		if (!entityBox.Trigger && !destBox.Trigger)
			return false;
		if (entity->triggerObject != NULL)
		{
			entity->triggerObject->OnCollision(pentity);
		}
		if (pentity->triggerObject != NULL)
		{
			pentity->triggerObject->OnCollision(entity);
		}
	}
	return true;
}

void Physics::GetNearByEntities(Entity * entity, std::vector<Entity*>* relevantEntities, bool trigger)
{
	Location* entityLocation = g_game->LocationForWorldPosition(entity->position);

	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			Location* loc = worldMap->LocationAt(entityLocation->x + x, entityLocation->y + y);
			if (loc == NULL)
				continue;
			if (trigger)
			{ 
				if (loc->triggerObject != NULL)
					relevantEntities->push_back(loc->triggerObject->entity);
			}
			else
			{
				if (loc->locationObject != NULL)
				{
					relevantEntities->push_back(loc->locationObject->entity);
					
				}
				if (loc->wall != NULL)
				{
					relevantEntities->push_back(loc->wall->entity);
				}
			}
		
		}
	}
}

void Physics::RunTriggerCollision(Entity* entity)
{
	std::vector<Entity*> relevantEntities = std::vector<Entity*>();
	GetNearByEntities(entity, &relevantEntities,true);

	BoundingBox entityBox = entity->GetBoundingBox(entity->position);
	for (UINT i = 0; i < relevantEntities.size(); i++)
	{
		Entity* pentity = relevantEntities.at(i);
		if (pentity->entityType != EntityWall && pentity->entityType != EntityObject && pentity->entityType != EntityEnemy)
			continue;
		BoundingBox destBox = pentity->GetBoundingBox(pentity->position);
		bool collision = destBox.Intersects(entityBox);
		if (!collision)
			return;

		if (!entityBox.Trigger && !destBox.Trigger)
			return;
		if (entity->triggerObject != NULL)
		{
			entity->triggerObject->OnCollision(pentity);
		}
		if (pentity->triggerObject != NULL)
		{
			pentity->triggerObject->OnCollision(entity);
		}
	}
}

Physics::Physics(std::vector<Entity*>* worldEntities )
{
	gameEntities = worldEntities;
}

void Physics::SetWorldMap(WorldMap * worldMap)
{
	this->worldMap = worldMap;
}


Physics::~Physics()
{
}
