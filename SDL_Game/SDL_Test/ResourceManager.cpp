#include "ResourceManager.h"
#include "globals.h"
#include "Game.h"

bool ResourceManager::LoadResources()
{
	bool success = true;

	backgroundSurface = loadSurface("assets/Background.bmp");
	pirateSurface = loadSurface("assets/MainCharacter.png");
	pirateKingSurface = loadSurface("assets/KingCharacter.png");
	pirateDogSurface = loadSurface("assets/DogCharacter.png");
	playerSurface = loadSurface("assets/MainCharacterCthulu.png");
	FloorTileMapSurface = loadSurface("assets/FloorCompleteTile.png");
	DialogueBackgroundSurface = loadSurface("assets/DialogueBackground.png");
	PirateSurface = loadSurface("assets/PirateSmall.png");
	PirateKingBattleSurface = loadSurface("assets/PirateKing.png");
	DogBattleSurface = loadSurface("assets/PirateDog.png");
	ChestSurface = loadSurface("assets/Chest.png");
	BattlePlayerAttack = loadSurface("assets/PlayerAttack.png");
	Cursor = loadSurface("assets/Cursor.png");
	FightDialogueBackgroundSurface = loadSurface("assets/FightDialogueBackground.png");
	BattleBackgroundSurface = loadSurface("assets/BattleBackground.png");
	StairSurface = loadSurface("assets/Stair.png");
	GameOverSurface = loadSurface("assets/DeathAnimation.png");
	IntroImageSurface = loadSurface("assets/TitleScreen.png");
	WinImageSurface = loadSurface("assets/WinScreen.png");
	
	BattleCursorSurface = loadSurface("assets/TargetCursor.png");
	if (backgroundSurface == NULL
		|| playerSurface == NULL)
	{
		printf("Unable to load images! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	SetTextBlocks();
	return success;
}


ResourceManager::ResourceManager()
{
	SetSoureRects();
}


Sprite ResourceManager::GetSpriteForId(SpriteId spriteId)
{
	Sprite sprite = Sprite();
	switch (spriteId)
	{
	case SpriteId::StairsDown:sprite.SourceSurface = FloorTileMapSurface; sprite.SourceRect = &sourceRects[4][0]; break;
	case SpriteId::StairsUp:sprite.SourceSurface = FloorTileMapSurface; sprite.SourceRect = &sourceRects[3][0]; break;
	case SpriteId::EnemyPirate:sprite.SourceSurface = pirateSurface; sprite.SourceRect = &sourceRects[0][0]; break;
	case SpriteId::EnemyPirateKing:sprite.SourceSurface = pirateKingSurface; sprite.SourceRect = &sourceRects[0][0]; break;
	case SpriteId::EnemyDog:sprite.SourceSurface = pirateDogSurface; sprite.SourceRect = &sourceRects[0][0]; break;
	case SpriteId::BattlePirate:sprite.SourceSurface = PirateSurface; sprite.SourceRect = NULL; break;
	case SpriteId::BattlePirateKing:sprite.SourceSurface = PirateKingBattleSurface; sprite.SourceRect = NULL; break;
	case SpriteId::BattleDog:sprite.SourceSurface = DogBattleSurface; sprite.SourceRect = NULL; break;
	case SpriteId::GameOverDeath:sprite.SourceSurface = GameOverSurface; sprite.SourceRect = NULL; break;
	default:
		break;
	}
	return sprite;
}

Animation* ResourceManager::GetAnimationForId(AnimationId animationId)
{
	switch (animationId)
	{
		case AnimationId::GameOverAnimation:return new Animation(GameOverSurface, 48, 48, { 0,1,2,3,4,5 },false);
		case AnimationId::PlayerDown:return new Animation(g_game->resourceManager->playerSurface, g_game->TILE_SIZE, g_game->TILE_SIZE, { 1,2 });
		case AnimationId::PlayerUp:return new Animation(g_game->resourceManager->playerSurface, g_game->TILE_SIZE, g_game->TILE_SIZE, { 3,4 });
		case AnimationId::PlayerLeft:return new Animation(g_game->resourceManager->playerSurface, g_game->TILE_SIZE, g_game->TILE_SIZE, { 5,6 });
		case AnimationId::PlayerRight:return new Animation(g_game->resourceManager->playerSurface, g_game->TILE_SIZE, g_game->TILE_SIZE, { 7,8 });
		case AnimationId::BattlePlayAttack:return  new Animation(g_game->resourceManager->BattlePlayerAttack, 16, 16, { 0,1,2,3,4,5,6,7,8 }, false);
		case AnimationId::BattleCursor:return  new Animation(g_game->resourceManager->BattleCursorSurface, 32, 60, { 0,1 });
		case AnimationId::IntroAnimation:return  new Animation(g_game->resourceManager->IntroImageSurface, 160, 144, { 0,1,2,3});
		case AnimationId::WinAnimation:return  new Animation(g_game->resourceManager->WinImageSurface, 160, 144, { 0,1,2,3 });
		default:
			break;
	}
	return NULL;
}

SDL_Surface* ResourceManager::loadSurface(std::string path)
{

	SDL_Surface* optimizedSurface = NULL;
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		optimizedSurface = SDL_ConvertSurface(loadedSurface, g_game->screenSurface->format, NULL);
		if (optimizedSurface == NULL)
		{
			printf("Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}
		SDL_FreeSurface(loadedSurface);
	}

	SDL_SetColorKey(optimizedSurface, SDL_TRUE, SDL_MapRGB(optimizedSurface->format, 0, 0xFF, 0xFF));
	return optimizedSurface;
}
void ResourceManager::SetSoureRects()
{
	for (int x = 0; x < MAX_TILE_PER_SHEET; x++)
	{
		for (int y = 0; y < MAX_TILE_PER_SHEET; y++)
		{
			sourceRects[x][y] = { x * SOURCE_TILE_SIZE,y * SOURCE_TILE_SIZE,SOURCE_TILE_SIZE,SOURCE_TILE_SIZE };
		}
	}
}

void ResourceManager::SetTextBlocks()
{
	IntroText =
		"Alas pirates have"
		"\ninvaded your sacred"
		"\nwaters and disturbed"
		"\nthe tranquil life of"
		"\nthe sea creatures"
		"\n"
		"\nYou must fight your way"
		"\nthrough them and\n"
		"\ndefeat the Pirate King"
		"\nto restore peace\n"
		"to your kingdom";

	OutroText =
		"..and so with the Pirate"
		"\nKing defeated the"
		"\nocean creatures were"
		"\nfinally left in peace"
		"\nagain and you are free"
		"\nto return to your"
		"\nslumber.."
		"\n"
		"\nuntil you are needed"
		"\nagain"
		"\n"
		"\n        THE END   ";
}
ResourceManager::~ResourceManager()
{

}
