#include "BoundingBox.h"
#include "Game.h"
#include "globals.h"


BoundingBox::BoundingBox()
{
	OffSetPosition = Vector2();
	Width = 16.0f;
	Height = 16.0f;
	Trigger = false;
}

bool BoundingBox::Intersects(BoundingBox otherBox)
{
	return (abs(position.x - otherBox.position.x) * 2 < (Width + otherBox.Width)) &&
		(abs(position.y - otherBox.position.y) * 2 < (Height + otherBox.Height));	 
}
BoundingBox::~BoundingBox()
{
}
