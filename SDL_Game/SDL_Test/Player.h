#pragma once
#include "globals.h"
#include "Direction.h"

class Entity;
class Player
{
public:
	Player();
	~Player();
	Entity* entity;
	float Speed = 40.0f;
	void ClearInput();
	void SetInput(MovementDirection dirn);
	MovementDirection currentDirection;
private:
	
};

