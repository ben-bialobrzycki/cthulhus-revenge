#pragma once
#include "Entity.h"

enum WallDirection {WDTopLeft, WDTop, WDTopRight, WDRight, WDBottomRight, WDBottom, WDBottomLeft, WDLeft,WDInnerTopLeft,WDInnerTopRight,WDInnerBottomRight,WDInnerBottomLeft, WDNone};
class Wall
{
public:
	Wall(int x, int y, WallDirection walldirn);
	Entity* entity;
	WallDirection direction;
	SDL_Rect* SourceRectForChar(WallDirection walldir);
	~Wall();
private:
	SDL_Rect* sourceRect;
};

