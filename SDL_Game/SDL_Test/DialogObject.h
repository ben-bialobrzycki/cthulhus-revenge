#pragma once
#include "Common.h"
#include "BufDefs.h"
class InteractiveObject;

class DialogObject
{
public:
	DialogObject(InteractiveObject* initiatingObject);
	InteractiveObject* initiatingObject;
	char message[STR_BUF_SIZE];
	~DialogObject();
};

