#include "Wall.h"
#include "Game.h"
#include "globals.h"


Wall::Wall(int x, int y, WallDirection walldirn)
{
	direction = walldirn;
	sourceRect = SourceRectForChar(direction);
	entity = new Entity();
	entity->entityType = EntityWall;
	entity->SetPosition(g_game->CoordinateToWorldPosition(x, y));
	entity->SetSprite(g_game->resourceManager->FloorTileMapSurface, sourceRect);
}



SDL_Rect* Wall::SourceRectForChar(WallDirection walldir)
{
	SDL_Rect* rect = new SDL_Rect();
	rect->w = rect->h = g_game->TILE_SIZE;
	int x, y;
	switch (walldir)
	{
	case '0':x = 1; y = 1; break; //Floor Tile
	case  WDLeft :x = 0; y = 1; break; //Left Wall
	case WDRight:x = 2; y = 1; break; //Right Wall
	case WDTop:x = 1; y = 0; break; //Top Wall
	case WDBottom:x = 1; y = 2; break; //Bottom Wall
	case WDTopLeft:x = 0; y = 0; break; //Top Left Corder
	case WDTopRight:x = 2; y = 0; break; //Top Right Corder
	case WDBottomRight:x = 2; y = 2; break; //Bottom Right Corder
	case WDBottomLeft:x = 0; y = 2; break; //Bottom Right Corder
	case WDInnerTopLeft:x = 3; y = 1; break; //Bottom Left Corder
	case WDInnerTopRight:x = 4; y = 1; break; //Bottom Left Corder
	case WDInnerBottomRight:x = 4; y = 2; break; //Bottom Left Corder
	case WDInnerBottomLeft:x = 3; y = 2; break; //Bottom Left Corder
	default: x = 1; y = 1; break;
		break;
	}
	rect->x = x *  g_game->TILE_SIZE;
	rect->y = y *  g_game->TILE_SIZE;
	return rect;
}

Wall::~Wall()
{
	delete sourceRect;
	delete entity;
}
