#pragma once
#include "Common.h"
#include "MagicSpell.h"
#include "DialogueCommands.h"
#include "MathUtility.h"


struct PlayerStat
{
	int MaxHitPoints;
	int MaxManaPoints;
	int AttackMin;
	int AttackMax;
};
class PlayerCharacter
{
public:
	PlayerCharacter();
	~PlayerCharacter();
	void TakeDamage(int roll);
	int HitPoints;
	//int MaxHitPoints;

	PlayerStat Stats;
	int ManaPoints;
	//int MaxManaPoints;

	int Level;
	
	char Name[50];
	std::vector<Spell> AvailableSpells;

	std::vector<DialogueOption> FightOptions;
	std::vector<DialogueOption> MagicOptions;

	void Heal(int amount);
	void AddMP(int amount);

	int GetAttackValue();
	
private:
	
	
};

