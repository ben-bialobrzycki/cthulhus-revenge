#include "Battle.h"
#include "InteractiveObject.h"
#include "StringUtil.h"
#include "SoundSystem.h"

Battle::Battle(DialogueManager* dialogueManager, PlayerParty* playerParty, EnemyGroupInfo* enemyGroupInfo)
{
	state = BattleState::PlayerInput;
	this->enemyGroupInfo = enemyGroupInfo;
	this->playerParty = playerParty;
	this->dialogueManager = dialogueManager;
	SetEnemies();
	animationController = new AnimationController();
	animationController->AddAnimation((int)AnimationId::BattlePlayAttack, g_game->resourceManager->GetAnimationForId(AnimationId::BattlePlayAttack));
	animationController->AddAnimation((int)AnimationId::BattleCursor, g_game->resourceManager->GetAnimationForId(AnimationId::BattleCursor));

	timer = std::make_unique<Timer>();
	isFinished = false;
	currentAnimationArea = { 0,0,0,0 };
	selectedEnemyTarget = 0;
}

void Battle::SetEnemies()
{
	enemies = std::vector<BattleEnemy>();
	SetOneEnemy(this->enemyGroupInfo->slot1Enemy);
	SetOneEnemy(this->enemyGroupInfo->slot2Enemy);
	SetOneEnemy(this->enemyGroupInfo->slot3Enemy);
}

void Battle::SetOneEnemy(EnemyType enemyType)
{
	EnemyInfo* enemyInfo = NULL;
	for (UINT i = 0; AllEnemies[i].enemyType != EnemyType::None; i++) {
		if (AllEnemies[i].enemyType == enemyType) {
			enemyInfo = &AllEnemies[i];
			break;
		}
	}
	if (enemyInfo == NULL)
		return;
	BattleEnemy enemy = BattleEnemy(enemyInfo);
	enemies.push_back(enemy);
}

void Battle::Initialise()
{
	g_game->soundSystem->PlayMusic(Music::Battle);
	state = BattleState::EnemyMessageShow;
	dialogueManager->SetTextForSlowMessage("A Battle Has Begun");
}

void Battle::AddMessageToBuffer(char* message)
{
	dialogueManager->AddTextForSlowMessage(message);
}

void Battle::Update()
{
	timer->Update();
	animationController->Update();

	if (state == BattleState::EnemyInput)
	{
		RunEnemyTurn();
		state = BattleState::EnemyMessageShow;
	}
	else if (state == BattleState::PlayerInput)
	{
		//TODO tidy up copy paste Dialogue
		DialogueInputResult dialogueResult = dialogueManager->HandleInput();
		if (dialogueResult.clicked)
		{
			switch (dialogueResult.command)
			{
			case Attack:
				currentAction = CurrentTargetAction::AttackTarget;
				state = BattleState::PlayerTargetChoose;
				break; //RunPlayerAttack(); break;
			case Defend: RunPlayerDefend(); break;
			case MagicChoose: dialogueManager->SetCurrentMenu(MagicChoice, playerParty, &(playerParty->playerCharacter->MagicOptions)); break;
			case ItemChoose: dialogueManager->SetCurrentMenu(ItemChoice, playerParty, &(playerParty->ItemOptions)); break;
			case DialogueUseItem:
			{
				RunPlayerUseItem((ItemType)dialogueResult.commandParameter);
			}break;
			case DialogueUseSpell:
			{
				RunPlayerUseSpell((Spell)dialogueResult.commandParameter);
			}break;
			case QuitDialogue:
			{
				if (dialogueResult.selectedMenu != FightChoice)
					dialogueManager->SetCurrentMenu(FightChoice, playerParty, &(playerParty->playerCharacter->FightOptions));
				break;
			}
			default:
				break;
			}
		}

	}
	else if (state == BattleState::PlayerTargetChoose)
	{
		if (enemies[selectedEnemyTarget].IsDead) {
			selectedEnemyTarget = 0;
			MoveTargetToNextPossible(true);
		}

		if (g_Input.LeftPressed && selectedEnemyTarget > 0)
		{
			MoveTargetToNextPossible(false);
		}
		if (g_Input.RightPressed && selectedEnemyTarget < (int)(enemies.size() - 1))
		{
			MoveTargetToNextPossible(true);
		}
		if (g_Input.APressed)
		{
			animationController->ClearCurrent();
			switch (currentAction)
			{
			case CurrentTargetAction::AttackTarget: UseAttackForTarget(); break;
			case CurrentTargetAction::UseSpellForTarget: UseSpellForTarget(); break;
			case CurrentTargetAction::UseItemForTarget: UseItemForTarget(); break;

			}
			return;
		}
		if (g_Input.BPressed)
		{
			animationController->ClearCurrent();
			SetForPlayerInput();
			return;
		}
		animationController->PlayAnimation((int)AnimationId::BattleCursor);
		//TODO tidy
		SDL_Rect cursor = { (enemyRectPosition1.x - 2) + (selectedEnemyTarget * BattleSpriteOffset),enemyRectPosition1.y - 2,32,60 };
		currentAnimationArea = cursor;
	}
	else if (state == BattleState::EnemyMessageShow || state == BattleState::PlayerMessageShow)
	{
		dialogueManager->Update();
		if (dialogueManager->finishedDisplayText)
		{
			if (playerParty->playerCharacter->HitPoints <= 0)
				RunPlayerDeath();
			else
			{
				if (state == BattleState::EnemyMessageShow)
					SetForPlayerInput();
				if (state == BattleState::PlayerMessageShow)
					state = BattleState::EnemyInput;
			}
		}
	}
}

void Battle::MoveTargetToNextPossible(bool right)
{
	int next;
	int increment;
	if (right)
		increment = 1;
	else
		increment = -1;
	next = selectedEnemyTarget + increment;
	while (next >= 0 && (UINT)next < enemies.size() && enemies[next].IsDead)
	{
		next += increment;
	}
	if (next >= 0 && (UINT)next < enemies.size())
	{
		selectedEnemyTarget = next;
	}
}

bool Battle::EnemiesLeft() {
	for (UINT i = 0; i < enemies.size(); i++) {
		if (!enemies[i].IsDead)
			return true;
	}
	return false;
}
void Battle::SetForPlayerInput()
{
	if (!EnemiesLeft()) {
		RunVictory();
		return;
	}

	dialogueManager->SetCurrentMenu(FightChoice, playerParty, &(playerParty->playerCharacter->FightOptions));
	state = BattleState::PlayerInput;
}

void Battle::RenderEnemies()
{
	//TODO Tidy
	SDL_Rect background = { 0,0,160,74 };
	SDL_BlitSurface(g_game->resourceManager->BattleBackgroundSurface, NULL, g_game->screenSurface, &background);
	for (UINT i = 0; i < enemies.size(); i++) {
		if (enemies[i].IsDead)
			continue;
		Sprite sprite = g_game->resourceManager->GetSpriteForId(enemies[i].enemyInfo->battleSprite);
		SDL_Rect destRect = enemyRectPosition1;
		destRect.x += i * BattleSpriteOffset;
		SDL_BlitSurface(sprite.SourceSurface, sprite.SourceRect, g_game->screenSurface, &destRect);
	}
}

void Battle::RenderCurrentAnimation()
{
	if (currentAnimationArea.w > 0)
	{
		Sprite sprite = animationController->GetCurrentSprite();
		SDL_BlitSurface(sprite.SourceSurface, sprite.SourceRect, g_game->screenSurface, &currentAnimationArea);
	}
}

void Battle::Render()
{
	RenderEnemies();
	RenderCurrentAnimation();
	if (state == BattleState::PlayerInput)
		dialogueManager->RenderPlayerUI(playerParty);
	else if (state != BattleState::PlayerTargetChoose)
	{
		dialogueManager->RenderDialogueBackground();
		dialogueManager->Render();
	}
}


BattleEnemy* Battle::GetSelectedEnemy()
{
	if (enemies.size() <= 0)
		return NULL;

	if (selectedEnemyTarget > (int)(enemies.size()) - 1)
		selectedEnemyTarget = (int)(enemies.size()) - 1;

	BattleEnemy* enemy = &enemies[selectedEnemyTarget];
	return enemy;
}

void Battle::PlayOverlayEnemyAnimation(AnimationId animationId)
{
	animationController->Reset();
	animationController->PlayAnimation((int)animationId);
	SDL_Rect targetEnemyArea = enemyRectPosition1;
	targetEnemyArea.x += (selectedEnemyTarget * BattleSpriteOffset);
	currentAnimationArea = MathUtility::CentreForSize(targetEnemyArea, 16, 16);
}

void Battle::RunPlayerAttack()
{
	char messageBuf[STR_BUF_SIZE + 1];
	BattleEnemy* enemy = GetSelectedEnemy();
	
	int roll = playerParty->playerCharacter->GetAttackValue();
	enemy->TakeDamage(roll);

	snprintf(messageBuf, STR_BUF_SIZE, "\n%s Does %d Damage", playerParty->playerCharacter->Name, roll);
	AddMessageToBuffer(messageBuf);
	if (enemy->IsDead) {

		snprintf(messageBuf, STR_BUF_SIZE, "\nEnemy %s Defeated", enemy->enemyInfo->Name);
		AddMessageToBuffer(messageBuf);
	}
	PlayOverlayEnemyAnimation(AnimationId::BattlePlayAttack);


	FinishPlayerTurn();

}

void Battle::RunPlayerDefend()
{
	char messageBuf[STR_BUF_SIZE + 1];
	snprintf(messageBuf, STR_BUF_SIZE, "\n%s Is Defending", playerParty->playerCharacter->Name);
	AddMessageToBuffer(messageBuf);
	FinishPlayerTurn();
}

void Battle::RunPlayerUseItem(ItemType itemType)
{
	char messageBuf[STR_BUF_SIZE + 1];

	currentItem = FindItemForType(itemType);
	UseItem(currentItem, playerParty, false);
	snprintf(messageBuf, STR_BUF_SIZE, "\n%s Used %s", playerParty->playerCharacter->Name, currentItem->ItemName);
	AddMessageToBuffer(messageBuf);
	FinishPlayerTurn();
}


void Battle::UseSpellForTarget()
{
	char messageBuf[STR_BUF_SIZE + 1];

	if (currentMagicSpell->NonCombatOnly)
		return;

	BattleEnemy* enemy = GetSelectedEnemy();
	int roll = g_game->GetRandomInt(currentMagicSpell->DamageMin, currentMagicSpell->DamageMax);
	enemy->TakeDamage(roll);
	playerParty->playerCharacter->AddMP(-currentMagicSpell->MPCost);

	snprintf(messageBuf, STR_BUF_SIZE, "\n%s Uses Spell %s\nDoes %d Damage", playerParty->playerCharacter->Name, currentMagicSpell->SpellName, roll);
	AddMessageToBuffer(messageBuf);
	if (enemy->IsDead) {

		snprintf(messageBuf, STR_BUF_SIZE, "\nEnemy %s Defeated", enemy->enemyInfo->Name);
		AddMessageToBuffer(messageBuf);
	}

	PlayOverlayEnemyAnimation(AnimationId::BattlePlayAttack);
	FinishPlayerTurn();
}

void Battle::UseAttackForTarget()
{
	RunPlayerAttack();
}

void Battle::UseItemForTarget()
{
	//TODO
}

void Battle::RunPlayerUseSpell(Spell spell)
{
	currentMagicSpell = FindSpell(spell);
	if (currentMagicSpell->MPCost > playerParty->playerCharacter->ManaPoints)
	{
		PlayErrorChoiceSound();
		return;
	}
	if (currentMagicSpell->NeedTarget)
	{
		state = BattleState::PlayerTargetChoose;
		currentAction = CurrentTargetAction::UseSpellForTarget;
		return;
	}
	UseSpellForTarget();

}

void Battle::FinishPlayerTurn()
{
	state = BattleState::PlayerMessageShow;

	/*timer->Start(2.00, [&]() {
		bool enemiesLeft = false;
		for (auto& enemy : enemies) {
			if (!enemy.IsDead)
				enemiesLeft = true;
		}
		if (enemiesLeft)
			state = BattleState::EnemyInput;
		else
			RunVictory();


	});*/
}



void Battle::RunVictory()
{
	state = BattleState::PlayerVictory;
	AddMessageToBuffer("\nVictory is Yours");
	timer->Start(2.0, [&] { isFinished = true; });
	g_game->soundSystem->StopMusic();
}

void Battle::RunPlayerDeath()
{
	state = BattleState::PlayerDeath;
	AddMessageToBuffer("\nYou have fallen");
	timer->Start(2.0, [&] { isFinished = true; });
	g_game->soundSystem->StopMusic();

}
void Battle::RunEnemyTurn()
{
	char messageBuf[STR_BUF_SIZE];
	for (UINT i = 0; i < enemies.size(); i++)
	{
		if (enemies[i].IsDead)
			continue;
		enemies[i].RunAttack(playerParty, messageBuf);
		AddMessageToBuffer(messageBuf);
	}

	
	/*for (auto&& e : enemies)
	{
		if (e.IsDead)
			continue;
		e.RunAttack(playerParty, messageBuf);
		AddMessageToBuffer(messageBuf);
	}*/

}


bool Battle::Finished()
{
	return isFinished;
}

Battle::~Battle()
{
	for (auto const& x : animationController->animations)
	{
		delete(x.second);
	}
}
