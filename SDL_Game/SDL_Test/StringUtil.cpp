#include "defs.h"
#include "StringUtil.h"
#include "globals.h"
#include "Common.h"
#include "DialogueManager.h"

void ItemListToStringList(std::vector<InventoryItem>* inventory, std::vector<std::string>* stringList)
{
	for (auto& item : *inventory)
	{
		if (item.Quanity <= 0)
			continue;
		char* itemName = NULL;
		for (int i = 0; AllItems[i].Type != ItemType::NullItem; i++) {
			if (AllItems[i].Type == item.itemType)
				itemName = AllItems[i].ItemName;
		}
		if (itemName == NULL)
			continue;
		std::ostringstream line = std::ostringstream();
		line << itemName << " " << item.Quanity;
		stringList->push_back(line.str());
	
	}
}
void SpellListToStringList(std::vector<Spell>* AvailableSpells, std::vector<std::string>* stringList)
{
	for (auto& spell :*AvailableSpells)
	{
		char* spellName = NULL;
		for (int i = 0; AllSpells[i].Spell != Spell::NullSpell; i++) {
			if (AllSpells[i].Spell == spell)
				spellName = AllSpells[i].SpellName;
		}
		if (spellName == NULL)
			continue;
	
		stringList->push_back(std::string(spellName));
	}
}

void BuildDialogueListItems(std::vector<InventoryItem>* inventory, std::vector<DialogueOption>* commands)
{

	for (UINT aspell = 0; aspell < inventory->size(); aspell++)
	{
		auto item = (*inventory)[aspell];
		if (item.Quanity <= 0)
			continue;
		char* itemName = NULL;
		for (int i = 0; AllItems[i].Type != ItemType::NullItem; i++) {
			if (AllItems[i].Type == item.itemType)
				itemName = AllItems[i].ItemName;
		}
		if (itemName == NULL)
			continue;
		
		DialogueOption option = { itemName,item.Quanity, DialogueUseItem, (int)item.itemType };
		commands->push_back(option);
	}

}

void BuildDialogueListSpells(std::vector<Spell>* AvailableSpells, std::vector<DialogueOption>* commands)
{
	for (UINT aspell=0; aspell < AvailableSpells->size(); aspell++)
	{
		char* spellName = NULL;
		for (int i = 0; AllSpells[i].Spell != Spell::NullSpell; i++) {
			if (AllSpells[i].Spell == AvailableSpells->at(aspell))
				spellName = AllSpells[i].SpellName;
		}
		if (spellName == NULL)
			continue;

		DialogueOption option = { spellName,0,DialogueUseSpell,(int)AvailableSpells->at(aspell)};
		commands->push_back(option);
	}
}