#pragma once
#include "AnimationController.h"

class TitleScreen
{
public:
	TitleScreen();
	void Run();
	bool Finished;
	~TitleScreen();
private:
	AnimationController animationController;
};

