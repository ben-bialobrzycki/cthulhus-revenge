#pragma once
#include "Common.h"
class GameTime
{
public:
	GameTime();
	void Update();
	float DeltaTime; //In Seconds
	float TotalTime; //In Seconds
	Uint32 TotalTicks; //Ticks == Ms
	~GameTime();
};

