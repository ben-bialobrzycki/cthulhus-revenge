#include "PlayerCharacter.h"
#include "StringUtil.h"
#include "globals.h"
#include "Game.h"


PlayerCharacter::PlayerCharacter()
{
	Stats = { 200,20,1,10 };
	HitPoints = Stats.MaxHitPoints;
	ManaPoints = Stats.MaxManaPoints;
	Level = 1;
	strcpy(Name, "Cthulhu");
	AvailableSpells = std::vector<Spell>();
	AvailableSpells.push_back(Spell::Ink);
	FightOptions = std::vector<DialogueOption>();
	
	FightOptions.push_back({ "Attack",0, Attack,0 });
	FightOptions.push_back({ "Defend",0, Defend,0 });
	FightOptions.push_back({ "Magic",0, MagicChoose,0 });
	FightOptions.push_back({ "Item",0, ItemChoose,0 });

	MagicOptions = std::vector<DialogueOption>();
	BuildDialogueListSpells(&AvailableSpells, &MagicOptions);

}



PlayerCharacter::~PlayerCharacter()
{
}

void PlayerCharacter::TakeDamage(int roll)
{
	HitPoints = MathUtility::ClampInt(HitPoints - roll, 0, Stats.MaxHitPoints);
}

void PlayerCharacter::Heal(int amount)
{
	HitPoints = MathUtility::ClampInt(HitPoints + amount,0,Stats.MaxHitPoints);
}

void PlayerCharacter::AddMP(int amount)
{
	ManaPoints = MathUtility::ClampInt(ManaPoints + amount, 0, Stats.MaxManaPoints);
}

int PlayerCharacter::GetAttackValue()
{
	return g_game->GetRandomInt(Stats.AttackMin, Stats.AttackMax);
}
