#pragma once
#include "AnimationController.h"
class WinState
{
public:
	WinState();
	void Run();
	void Initialise();
	~WinState();
private:
	AnimationController animationController;
};

