
#pragma once
#include "Vector2.h"
#include "Common.h"
#include "BoundingBox.h"
#include "AnimationController.h"
#include "MathUtility.h"

class Game;
class GameTime;
class TriggerObject;

enum EntityType {EntityUnknown, EntityPlayer, EntityWall, EntityEnemy, EntityObject};
class Entity
{
public:
	Entity();
	void Update();
	void Render();
	void SetSprite(SDL_Surface* sprite, SDL_Rect* sourcRect);
	void SetSprite(Sprite sprite);
	void SetPosition(float x, float y);
	void SetPosition(Vector2 positionVector);
	
	Vector2 position;
	Vector2 velocity;
	BoundingBox GetBoundingBox(Vector2 position);
	int width;
	int height;
	EntityType entityType;
	AnimationController* animationController;
	TriggerObject* triggerObject;
	bool Active;
	~Entity();
private:
	
	Sprite* staticSprite;

};

typedef Entity* pEntity;