#include "GameTime.h"


GameTime::GameTime()
{
	TotalTicks = 0;
	DeltaTime = 0.0f;
	TotalTime = 0.0f;
}

void GameTime::Update()
{
	Uint32 total = SDL_GetTicks();
	TotalTime = total / 1000.0f;
	DeltaTime = (total - TotalTicks) / 1000.0f;
	TotalTicks = total;
}

GameTime::~GameTime()
{
}
