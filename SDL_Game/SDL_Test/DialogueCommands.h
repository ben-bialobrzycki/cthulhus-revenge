//#pragma once

#ifndef DIALOGUE_COMMANDS
#define DIALOGUE_COMMANDS
enum DialogueCommand { NullCommand,Attack,Defend, MagicChoose,ItemChoose,DialogueUseSpell, DialogueUseItem, QuitDialogue};

enum MenuName { FightChoice,MagicChoice,ItemChoice };

struct DialogueOption
{
	char* OptionNarr;
	int OptionNumber;
	DialogueCommand CommandId;
	int commandParameter;
};

struct DialogueInputResult
{
	bool clicked;
	int selectedItemIndex;
	MenuName selectedMenu;
	bool closeMenu;
	DialogueCommand command;
	int commandParameter;
};
#endif