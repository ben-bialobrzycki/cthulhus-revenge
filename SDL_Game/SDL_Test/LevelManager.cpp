#include "LevelManager.h"
#include "globals.h"
#include "Game.h"
#include "Location.h"


LevelManager::LevelManager()
{
	walls = std::vector<Wall*>();
}


void LevelManager::Initialise()
{
	
	 levels = std::vector<Level>();

	AddLevel(tilemap1, LEVEL1_HEIGHT, LEVEL1_WIDTH, enemies1, chests1);
	AddLevel(tilemap2, LEVEL1_HEIGHT, LEVEL1_WIDTH, enemies2, chests2);
	AddLevel(tilemap3, LEVEL1_HEIGHT, LEVEL1_WIDTH, enemies3, chests3);
	AddLevel(tilemap4, LEVEL1_HEIGHT, LEVEL1_WIDTH, enemies4, chests4);
	levelIndex = 0;
	currentLevel = &levels[levelIndex];
	
}

void LevelManager::AddLevel(char tilemap[LEVEL1_HEIGHT][LEVEL1_WIDTH], int height, int width, EnemyGroupInfo* levelEnemies, ChestInfo* levelChests)
{
	Level level = Level();
	level.tiles = new std::vector<std::vector<char>*>();
	level.worldMap = new WorldMap(height, width);

	for (int y = 0; y < height; y++)
	{
		std::vector<char>* row = new std::vector<char>();
		for (int x = 0; x < width; x++)
		{
			row->push_back(tilemap[y][x]);
		}
		level.tiles->push_back(row);
	}

	SetLevelObjects(&level,levelEnemies,levelChests);
	levels.push_back(level);

}

WallDirection LevelManager::IsWall(char c)
{
	switch (c)
	{
	case '0':return WDNone;
	case 'L':return WDLeft;
	case 'R':return WDRight;
	case 'T':return WDTop;
	case 'B':return WDBottom;
	case '1':return WDTopLeft;
	case '2':return WDTopRight;
	case '3':return WDBottomRight;
	case '4':return WDBottomLeft;
	case '5':return WDInnerTopLeft;
	case '6':return WDInnerTopRight;
	case '7':return WDInnerBottomRight;
	case '8':return WDInnerBottomLeft;
	default: return WDNone;
		break;
	}
	return WDNone;
}
InteractiveObject* LevelManager::InteractiveObjectForChar(char c, int x , int y)
{
	switch (c)
	{
		case'C' : return new InteractiveObject(IObjType::Chest,x,y);
		break;
	
		default: return NULL;
			break;
	}
	return NULL;
}

TriggerObject* LevelManager::TriggerObjectForChar(char c, int x, int y)
{
	switch (c)
	{
	case'U': return new TriggerObject(TrigObjType::StairsUp, x, y);
		case'D': return new TriggerObject(TrigObjType::StairsDown, x, y);
	default:
		break;
	}
	return nullptr;
}

void LevelManager::SetLevelObjects(Level* level, EnemyGroupInfo* levelEnemies, ChestInfo* levelChests)
{
	level->entities = std::vector<pEntity>();
	for (UINT y = 0; y < level->tiles->size(); y++)
	{
		std::vector<char>* row = level->tiles->at(y);
		for (UINT x = 0; x < row->size(); x++)
		{
			WallDirection walldirn = IsWall(row->at(x));
			if (walldirn != WDNone)
			{
				Wall* wall = new Wall(x,y,walldirn);
				walls.push_back(wall);
				level->worldMap->LocationAt(x, y)->wall = wall;
				level->entities.push_back(wall->entity);
			}
			InteractiveObject* interactiveObject = InteractiveObjectForChar(row->at(x),x,y);
			if (interactiveObject != NULL)
			{
				level->worldMap->LocationAt(x, y)->locationObject = interactiveObject;
				level->entities.push_back(interactiveObject->entity);
			}
			TriggerObject* triggerObject = TriggerObjectForChar(row->at(x), x, y);
			if (triggerObject != NULL)
			{
				level->worldMap->LocationAt(x, y)->triggerObject = triggerObject;
				level->entities.push_back(triggerObject->entity);
			}
		}
	}

	for (UINT i = 0; levelEnemies[i].x >= 0; i++) {
		EnemyGroupInfo enemyInfo = levelEnemies[i];
		enemyInfo.Finished = false;
		InteractiveObject* interactiveObject = new InteractiveObject(IObjType::Enemy, enemyInfo.x, enemyInfo.y);
		interactiveObject->enemyInfo = &levelEnemies[i];
		interactiveObject->entity->SetSprite(g_game->resourceManager->GetSpriteForId(enemyInfo.groupSprite));
		Location* location = level->worldMap->LocationAt(enemyInfo.x, enemyInfo.y);
		location->locationObject = interactiveObject;
		level->entities.push_back(interactiveObject->entity);
	}

	for (UINT i = 0; levelChests[i].x >= 0; i++) {
		ChestInfo chestInfo = levelChests[i];
		chestInfo.opened = false;
		InteractiveObject* interactiveObject = new InteractiveObject(IObjType::Chest, chestInfo.x, chestInfo.y);
		interactiveObject->chestInfo = levelChests[i];
		Location* location = level->worldMap->LocationAt(chestInfo.x, chestInfo.y);
		location->locationObject = interactiveObject;
		level->entities.push_back(interactiveObject->entity);
	}

}
void LevelManager::SetTileMap(SDL_Surface* sdl_sprite)
{
	tilemapsurface = sdl_sprite;
}

SDL_Rect LevelManager::DestRectForCoordinates(int x, int y)
{
	SDL_Rect rect = SDL_Rect();
	Vector2 renderPos = g_game->WorldPositionToRenderPosition((float)x * TILE_SIZE,(float) y * TILE_SIZE);
	rect.x = MathUtility::RoundFloat(renderPos.x);// x * TILE_SIZE * g_game->Scale;
	rect.y = MathUtility::RoundFloat(renderPos.y);// y * TILE_SIZE * g_game->Scale;
	rect.w =  TILE_SIZE * g_game->Scale;
	rect.h = TILE_SIZE * g_game->Scale;
	return rect;
}

SDL_Rect LevelManager::SourceRectForChar(char c)
{
	SDL_Rect rect = SDL_Rect();
	rect.w = rect.h = TILE_SIZE;
	int x, y;
	switch (c)
	{
		case '0':x = 1; y = 1; break; //Floor Tile
		case 'L':x = 0; y = 1; break; //Left Wall
		case 'R':x = 2; y = 1; break; //Right Wall
		case 'T':x = 1; y = 0; break; //Top Wall
		case 'B':x = 1; y = 2; break; //Bottom Wall
		case '1':x = 0; y = 0; break; //Top Left Corder
		case '2':x = 2; y = 0; break; //Top Right Corder
		case '3':x = 2; y = 2; break; //Bottom Right Corder
		case '4':x = 0; y = 2; break; //Bottom Left Corder
		case 'U':x = 3; y = 0; break; //Stairs Up
		case 'D':x = 4; y = 0; break; //Stairs Down
		case ' ':x = 5; y = 0; break; //Empty
		default: x = 5; y = 0; break;
			break;
	}
	rect.x = x * TILE_SIZE;
	rect.y = y * TILE_SIZE;
	return rect;
}
void LevelManager::RenderTileChar(int x, int y, char tilechar)
{
	SDL_Rect destRect = DestRectForCoordinates(x, y);
	SDL_Rect sourceRect = SourceRectForChar(tilechar);

	SDL_BlitSurface(this->tilemapsurface, &sourceRect, g_game->screenSurface, &destRect);
}
void LevelManager::Render()
{
	for (UINT y = 0; y < currentLevel->tiles->size(); y++)
	{
		std::vector<char>* row = currentLevel->tiles->at(y);
		for (UINT x = 0; x < row->size(); x++)
		{
			if (IsWall(row->at(x)) == WDNone)
				RenderTileChar(x, y, row->at(x));
		}
	}
}
bool LevelManager::ChangeLevel(UINT levelDiff)
{
	if (levelIndex + levelDiff < 0 || levelIndex + levelDiff > levels.size())
		return false;
	levelIndex += levelDiff;
	currentLevel = &levels[levelIndex];
	return true;
}
LevelManager::~LevelManager()
{
	//TODO should any clear up be done here?
	//for (UINT i = 0; i < levels.size(); i++)
	//{
		//delete( levels[i]->tiles);
		//delete(levels[i]->worldMap);
	//}
}
