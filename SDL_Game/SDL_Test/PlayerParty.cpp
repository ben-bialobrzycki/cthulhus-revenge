#include "PlayerParty.h"
#include "StringUtil.h"


PlayerParty::PlayerParty()
{
	playerCharacter = new PlayerCharacter();
	inventory = new std::vector<InventoryItem>();
	inventory->push_back({ ItemType::HealPotion,2 });
	ItemOptions = std::vector<DialogueOption>();
	BuildDialogueListItems(inventory, &ItemOptions);
	RefreshItemInfo();
	
	
}

void PlayerParty::RemoveInventoryItem(ItemType itemType)
{
	for (UINT i=0; i <inventory->size();i++)
	{
		if (inventory->at(i).itemType == itemType)
		{
			inventory->at(i).Quanity--;
			break;
		}
	}
	RefreshItemInfo();

}

void PlayerParty::RefreshItemInfo()
{
	ItemOptions.clear();
	BuildDialogueListItems(inventory, &ItemOptions);
}

PlayerParty::~PlayerParty()
{
	delete(playerCharacter);
	delete(inventory);
}
