#pragma once
//#include "Game.h"
#include "MagicSpell.h"
#include "Item.h"
#include "FrameInput.h"

class Game;
class GameTime;
struct EnemyInfo;
 extern Game* g_game;
 extern GameTime g_CurrentTime;
 extern FrameInput g_Input;
 extern FrameInput g_PrevInput;
 extern MagicSpell AllSpells[];
 extern Item AllItems[];
 extern EnemyInfo AllEnemies[];

 const int StairSize = 1;
 const int StairFinishOffset = 4;
