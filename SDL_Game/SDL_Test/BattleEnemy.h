#pragma once
#include "Common.h"
#include "globals.h"
#include "Game.h"
#include "PlayerParty.h"
#include "PlayerCharacter.h"
#include "BufDefs.h"
class BattleEnemy
{
public:
	BattleEnemy(EnemyInfo* enemyInfo);
	void RunAttack(PlayerParty* playerParty, char* messageBuf);
	void TakeDamage(int damage);
	~BattleEnemy();
	int HitPoints;
	bool IsDead;
	const EnemyInfo* enemyInfo;
private:
	
};

