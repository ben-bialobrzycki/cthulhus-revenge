#pragma once
class Vector2;
class Entity;
class WorldMap;
#include "Common.h"
#include "defs.h"

class Physics
{
public:
	Physics(std::vector<Entity*>* worldEntities);
	void SetWorldMap(WorldMap* worldMap);
	bool AllowedMovement(Vector2 nextposition, Entity* entity);
	void GetNearByEntities(Entity * entity, std::vector<Entity *>* relevantEntities, bool trigger);
	void RunTriggerCollision(Entity* entity);
	~Physics();
private:
	std::vector<Entity*>* gameEntities;
	WorldMap* worldMap;
};

