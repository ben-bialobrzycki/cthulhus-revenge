#pragma once
#include "Common.h"

enum class Music {None, TitleScreen,Overland,Battle,Victory, Death};
enum class SFX {DialogueCharBeep, DamageTaken, ShortBleep};
class SoundSystem
{
public:
	SoundSystem();
	void PlayMusic(Music music);
	void StopMusic();
	bool Initialise();

	Music currentMusic;
private:
#ifdef __EMSCRIPTEN__
#else
	Mix_Music* titleMusic = NULL;
	Mix_Music* battleMusic = NULL;
	Mix_Music* TryLoadMusicFile(char* fileName, bool* failFlag);

	Mix_Chunk* dialogueSound = NULL;
#endif
	~SoundSystem();
};

