#include "DialogueManager.h"
#include "globals.h"
#include "Game.h"
#include <strstream>
#include "StringUtil.h"

DialogueManager::DialogueManager()
{
	textSurface = NULL;
	gFont = NULL;
	messageTimer = Timer();
	lines = std::vector<std::string>();
	currentMessageIndex = 0;
}

bool DialogueManager::Initialise()
{
	bool success = true;
	gFont = TTF_OpenFont("assets/fonts/LiberationMono-Regular.ttf", 10);
	
	if (gFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}
	return success;
}

void DialogueManager::RenderText(char* msg, SDL_Color textColor, SDL_Rect rect, int lineSpacing)
{
	char* pmessage;
	strcpy(tempBuffer, msg);

	pmessage = strtok(tempBuffer, "\n");
	std::vector<char*> lines = std::vector<char*>();
	
	while (pmessage)
	{
		lines.push_back(pmessage);
		pmessage = strtok(NULL, "\n");
	}
	int line_count = lines.size();
	for (int i = line_count - messageBufferMax; i < line_count; i++)
	{
		if (i < 0)
			continue;
		textSurface = TTF_RenderText_Solid(gFont, lines[i], textColor);
		if (!textSurface)
			return;
		SDL_BlitSurface(textSurface, NULL, g_game->screenSurface, &rect);
		SDL_FreeSurface(textSurface);
		rect.y += lineSpacing;
		textSurface = NULL;
	}
}

void DialogueManager::RenderTextLine(const char* msg, SDL_Color textColor, SDL_Rect rect)
{
	textSurface = TTF_RenderText_Solid(gFont, msg, textColor);
	SDL_BlitSurface(textSurface, NULL, g_game->screenSurface, &rect);
	SDL_FreeSurface(textSurface);
	textSurface = NULL;
}

void DialogueManager::RenderCharTextLine(const char* msg, SDL_Color textColor, SDL_Rect rect)
{
	textSurface = TTF_RenderText_Solid(gFont, msg, textColor);
	SDL_BlitSurface(textSurface, NULL, g_game->screenSurface, &rect);
	SDL_FreeSurface(textSurface);
	textSurface = NULL;

}
void DialogueManager::Render()
{
	SDL_BlitScaled(g_game->resourceManager->DialogueBackgroundSurface, NULL, g_game->screenSurface, &messageBorderRectBottom);
	RenderText(currentVisibleMessage, color, messageTextRectBottom, 10);
}

void DialogueManager::RenderTop()
{
	RenderText(currentVisibleMessage, lightColor, messageTextRectTop, 10);
}

void DialogueManager::SetTextForSlowMessage(char* message)
{
	strncpy(currentMessage, message, STR_MESSAGE_BUF_SIZE -1);
	currentMessage[STR_MESSAGE_BUF_SIZE - 1] = '\0';
	currentMessageIndex = 0;
	finishedDisplayText = false;
	FastText = false;
	messageTimer.Start(secondsPerCharNormal, NULL);
}

void DialogueManager::AddTextForSlowMessage(char* message)
{
	if ((strlen(currentMessage) + strlen(message)) >= (STR_MESSAGE_BUF_SIZE -1))
	{
		
		int offset = STR_MESSAGE_BUF_SIZE / 2;
		int newEnd = strlen(currentMessage) - offset;
		strncpy(currentMessage, &currentMessage[offset], (STR_MESSAGE_BUF_SIZE - offset) - 1);
		currentMessageIndex = currentMessageIndex - offset; //= 0;// -= (int)(STR_MESSAGE_BUF_SIZE / 2);
		if (currentMessageIndex < 0)
			currentMessageIndex = 0;
		currentMessage[STR_MESSAGE_BUF_SIZE - 1] = '\0';
		currentMessage[newEnd] = '\0';
	/*	if (currentMessageIndex < 0)
			currentMessageIndex = 0;*/
	}
	if ((strlen(currentMessage) + strlen(message)) < STR_MESSAGE_BUF_SIZE - 1)
	{
		strcat(currentMessage, message);
		finishedDisplayText = false;
	}
	else
	{
		printf("Message list overflowed strlen(currentMessage) + %d strlen(message)  %d ", strlen(currentMessage),strlen(message));
	}
}

void DialogueManager::Update()
{
	if (g_Input.AIsDown)
		FastText = true;
	else
		FastText = false;

	messageTimer.Update();
	if (!finishedDisplayText && messageTimer.Finished)
		DoNextMessageChar();

	strncpy(currentVisibleMessage,currentMessage,currentMessageIndex);
	currentVisibleMessage[currentMessageIndex + 1] = '\0';
}

void DialogueManager::RenderDialogueBackground()
{
	
}

void DialogueManager::DoNextMessageChar()
{
	if ((UINT)currentMessageIndex >= strlen(currentMessage))
	{
		finishedDisplayText = true;
		return;
	}
	currentMessageIndex++;
	if (currentMessageIndex > STR_MESSAGE_BUF_SIZE - 1)
		currentMessageIndex = STR_MESSAGE_BUF_SIZE - 1;
	if (FastText)
		messageTimer.Start(secondsPerCharFast, NULL);
	else
		messageTimer.Start(secondsPerCharNormal, NULL);
}

void DialogueManager::RenderPlayerInfo(PlayerParty* playerParty)
{
	char lineBuf[STR_BUF_SIZE];
	SDL_BlitSurface(g_game->resourceManager->FightDialogueBackgroundSurface, NULL, g_game->screenSurface, &borderRectLeft);

	RenderTextLine(playerParty->playerCharacter->Name, color, textRectPlayerInfo);
	sprintf(lineBuf, "HP %d/%d", playerParty->playerCharacter->HitPoints, playerParty->playerCharacter->Stats.MaxHitPoints);
	SDL_Rect rectHP = textRectPlayerInfo;
	rectHP.y += lineSpacing;
	SDL_Rect rectMP = textRectPlayerInfo;
	rectMP.y += (2 * lineSpacing);
	RenderTextLine(lineBuf, color, rectHP);
	sprintf(lineBuf, "MP %d/%d", playerParty->playerCharacter->ManaPoints, playerParty->playerCharacter->Stats.MaxManaPoints);
	RenderTextLine(lineBuf, color, rectMP);
}

void DialogueManager::RenderItemMenu(int cursorSelectedIndex)
{
	SDL_Rect textRect = textRectRight;
	SDL_BlitSurface(g_game->resourceManager->FightDialogueBackgroundSurface, NULL, g_game->screenSurface, &borderRectRight);
	SDL_Rect cursorRectRight = { 90,86 + ((cursorSelectedIndex)* lineSpacing),16,8 };
	SDL_BlitSurface(g_game->resourceManager->Cursor, NULL, g_game->screenSurface, &cursorRectRight);
	
	
	for (auto& item : *currentMenuOptions)
	{
		const char* itemText;
		if (item.OptionNumber == 0)
			itemText = item.OptionNarr;
		else
		{
			snprintf(lineBuffer, STR_BUF_SIZE, "%s %d", item.OptionNarr, item.OptionNumber);
			itemText = lineBuffer;
		}

		RenderCharTextLine(itemText, color, textRect);
		textRect.y += lineSpacing;
	}

}


void DialogueManager::RenderPlayerUI(PlayerParty* playerParty)
{
	RenderItemMenu(cursorSelectedIndex);
	RenderPlayerInfo(playerParty);
}

DialogueInputResult DialogueManager::HandleInput()
{
	DialogueInputResult result = {};
	
	result.selectedItemIndex = cursorSelectedIndex;
	result.selectedMenu = currentMenu;

	if (g_Input.BPressed)
	{
		result.command = QuitDialogue;
		result.clicked = true;
	}
	if (g_Input.UpPressed)
	{
		if (cursorSelectedIndex > 0)
			cursorSelectedIndex--;
	}

	if (g_Input.DownPressed)
	{
		if (currentMenuOptions != NULL && (UINT)cursorSelectedIndex < currentMenuOptions->size() - 1)
			cursorSelectedIndex++;
	}

	if (g_Input.APressed)
	{
		result.clicked = true;
		if (currentMenuOptions && (UINT)cursorSelectedIndex < (*currentMenuOptions).size()) {
			result.command = (*currentMenuOptions)[cursorSelectedIndex].CommandId;
			result.commandParameter = (*currentMenuOptions)[cursorSelectedIndex].commandParameter;
		}
		
		
	}
	return result;
}

void DialogueManager::SetCurrentMenu(MenuName currentMenu, PlayerParty* playerParty, std::vector<DialogueOption>* FightOptions)
{
	this->currentMenu = currentMenu;
	this->currentMenuOptions = FightOptions;
	
	cursorSelectedIndex = 0;

}


DialogueManager::~DialogueManager()
{
}
