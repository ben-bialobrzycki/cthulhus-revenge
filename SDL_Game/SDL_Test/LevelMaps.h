#pragma once

#include "Item.h"
#include "InteractiveObject.h"
#include "InteractiveObjectTypes.h"

#define LEVEL1_HEIGHT 28
#define LEVEL1_WIDTH 20

#define MAX_LEVEL_ENEMIES 20;
#define MAX_LEVEL_ITEMS 20;

extern char tilemap1[LEVEL1_HEIGHT][LEVEL1_WIDTH];
extern char tilemap2[LEVEL1_HEIGHT][LEVEL1_WIDTH];
extern char tilemap3[LEVEL1_HEIGHT][LEVEL1_WIDTH];
extern char tilemap4[LEVEL1_HEIGHT][LEVEL1_WIDTH];


extern EnemyGroupInfo enemies1[];
extern ChestInfo chests1[];

extern EnemyGroupInfo enemies2[];
extern ChestInfo chests2[];

extern EnemyGroupInfo enemies3[];
extern ChestInfo chests3[];

extern EnemyGroupInfo enemies4[];
extern ChestInfo chests4[];
