#include "Player.h"
#include "Game.h"
#include "Entity.h"
#include "globals.h"

Player::Player()
{
	entity = new Entity();
	entity->entityType = EntityPlayer;
	entity->animationController = new AnimationController();
	entity->animationController->AddAnimation((int)AnimationId::PlayerDown, g_game->resourceManager->GetAnimationForId(AnimationId::PlayerDown));
	entity->animationController->AddAnimation((int)AnimationId::PlayerUp,g_game->resourceManager->GetAnimationForId(AnimationId::PlayerUp));
	entity->animationController->AddAnimation((int)AnimationId::PlayerLeft, g_game->resourceManager->GetAnimationForId(AnimationId::PlayerLeft));
	entity->animationController->AddAnimation((int)AnimationId::PlayerRight, g_game->resourceManager->GetAnimationForId(AnimationId::PlayerRight));
	
	entity->animationController->PlayAnimation((int)AnimationId::PlayerDown);
	entity->animationController->StopAnimation();
	//entity->animationController.
	entity->SetSprite(g_game->resourceManager->playerSurface, NULL);
}



void Player::ClearInput()
{

}
void Player::SetInput(MovementDirection dirn)
{
	//Keep on grid lines but allow one pixel lee way
	if (dirn == Up || dirn == Down)
	{
		int diff = MathUtility::RoundFloat(entity->position.x) % g_game->TILE_SIZE;


		if (diff >= 1)
		{
			dirn = currentDirection;
		}
		else
		{
			entity->position.x = (float)MathUtility::RoundFloat((float)entity->position.x);
		}
	}
	if (dirn == Left || dirn == Right)
	{
		int diff = MathUtility::RoundFloat(entity->position.y) % g_game->TILE_SIZE;
		if (diff >= 1)
			dirn = currentDirection;
		else
			entity->position.y = (float)MathUtility::RoundFloat((float)entity->position.y);
	}


	if (dirn != None)
		currentDirection = dirn;
	entity->velocity = Vector2::VectorForDirection(dirn).Multiply(Speed);
	switch (dirn)
	{
	case Up: entity->animationController->PlayAnimation((int)AnimationId::PlayerUp);
		break;
	case Down:entity->animationController->PlayAnimation((int)AnimationId::PlayerDown);
		break;
	case Left:entity->animationController->PlayAnimation((int)AnimationId::PlayerLeft);
		break;
	case Right:entity->animationController->PlayAnimation((int)AnimationId::PlayerRight);
		break;
	case None:entity->animationController->StopAnimation();
		break;
	default:
		break;
	}
	/*Vector2 nextPosition ;
	nextPosition.x = entity->position.x;
	nextPosition.y = entity->position.y;
	Vector2 vecMov = Vector2::VectorForDirection(dirn);
	vecMov.Multiply((g_game->Scale * entity->width));
	nextPosition = nextPosition.Add(vecMov);
	entity->SetPosition(nextPosition.x, nextPosition.y);*/
}


Player::~Player()
{
	delete entity;
}
