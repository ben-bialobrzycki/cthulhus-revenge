#include "BattleEnemy.h"



BattleEnemy::BattleEnemy(EnemyInfo* enemyInfo)
{
	IsDead = false;
	this->enemyInfo = enemyInfo;
	HitPoints = this->enemyInfo->HitPoints;
}

void BattleEnemy::RunAttack(PlayerParty * playerParty, char* messageBuf)
{
	int roll = g_game->GetRandomInt(enemyInfo->AttackMin, enemyInfo->AttackMax);
	playerParty->playerCharacter->TakeDamage(roll);
	snprintf(messageBuf, STR_BUF_SIZE, "\n%s Does %d Damage", enemyInfo->Name, roll);
}

void BattleEnemy::TakeDamage(int damage)
{
	HitPoints -= damage;
	if (HitPoints < 0) {
		HitPoints = 0;
		IsDead = true;
	}

}


BattleEnemy::~BattleEnemy()
{
}
