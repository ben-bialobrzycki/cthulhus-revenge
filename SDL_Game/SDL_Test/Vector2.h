#pragma once
#include "Direction.h"
#include "Common.h"
class Vector2
{
public:

	float x;
	float y;
	Vector2();
	Vector2(float xpos, float ypos);
	Vector2 Multiply(float size);
	float SquareLength();
	bool ApproxZero();
	Vector2 Add(Vector2 vec);
	static Vector2 VectorForDirection(MovementDirection dirn);
	std::string ToString();
	~Vector2();
};

