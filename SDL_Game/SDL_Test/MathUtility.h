#pragma once

#include "Common.h"

#ifdef __linux__ 
#include <SDL2/SDL.h>
#include <SDL2/SDL_rect.h>
#elif _WIN32
#include <SDL.h>
#endif

class MathUtility
{
public:
	MathUtility();
	static int RoundFloat(float x);
	static int ClampInt(int value, int min, int max);
	static SDL_Rect CentreForSize(SDL_Rect target, int width, int height);
	static SDL_Rect TopLeftForCentre(SDL_Rect target);
	~MathUtility();
};

