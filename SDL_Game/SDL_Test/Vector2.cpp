#include "Vector2.h"


Vector2 Vector2::VectorForDirection(MovementDirection dirn)
{
	Vector2 vec;
	switch (dirn)
	{
	case Up:
		vec.x = 0; vec.y = -1; break;
	case Down:
		vec.x = 0; vec.y = 1; break;
	case Left:
		vec.x = -1; vec.y = 0; break;
	case Right:
		vec.x = 1; vec.y = 0; break;
	case None:
		vec.x = 0; vec.y = 0; break;
	default:
		break;
	}
	return vec;

}

Vector2 Vector2::Multiply(float size)
{
	Vector2 result;
	result.x = x * size;
	result.y = y * size;
	return result;
	
}

Vector2 Vector2::Add(Vector2 vec)
{
	Vector2 ret;
	ret.x = x + vec.x;
	ret.y = y + vec.y;
	return ret;
}

Vector2::Vector2()
{
	x = y = 0.0f;
}

float Vector2::SquareLength()
{
	return (x * x) + (y * y);
}

bool Vector2::ApproxZero()
{
	return SquareLength() < 0.001f;
}

std::string Vector2::ToString()
{
	char buf[100];
	snprintf(buf, 100,"(%.2f, %.2f)", x, y);
	return std::string(buf);
}
Vector2::Vector2(float xpos, float ypos)
{
	x = xpos;
	y = ypos;
}


Vector2::~Vector2()
{
}
