#pragma once
#include "Animation.h"
#include "Sprite.h"
#include <map>
#include "defs.h"

class AnimationController
{
public:
	AnimationController();
	std::map<int, Animation*> animations;
	
	Sprite GetCurrentSprite();
	void PlayAnimation(int animationID);
	void Update();
	void AddAnimation(int animationId, Animation* animation);
	void StopAnimation();
	void Reset();
	void ClearCurrent();
	~AnimationController();
private:
	int currentAnimationId;
	float totalAnimationTime;
	bool stopped;
	Animation* currentAnimation;
};

