#include "WinState.h"
#include "Common.h"
#include "Game.h"


WinState::WinState()
{
	animationController = AnimationController();
	Animation* animation = g_game->resourceManager->GetAnimationForId(AnimationId::WinAnimation);
	animationController.AddAnimation((int)AnimationId::WinAnimation, animation);
	animationController.PlayAnimation((int)AnimationId::WinAnimation);
}

void WinState::Initialise()
{
	g_game->dialogueManager->SetTextForSlowMessage(g_game->resourceManager->OutroText);
}
void WinState::Run()
{
	animationController.Update();
	g_game->dialogueManager->Update();
	Sprite sprite = animationController.GetCurrentSprite();
	SDL_BlitSurface(sprite.SourceSurface, sprite.SourceRect, g_game->screenSurface, NULL);

	g_game->dialogueManager->RenderTop();

	if (g_Input.APressed && g_game->dialogueManager->finishedDisplayText)
	{
		g_game->SetNewPlayState(IntroScreen);
	}
}

WinState::~WinState()
{
}
