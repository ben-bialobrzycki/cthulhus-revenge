#include "Entity.h"
#include "Game.h"
#include "GameTime.h"
#include "globals.h"

extern Game* g_game;

Entity::Entity()
{
	width = 16;
	height = 16;
	position = Vector2();
	velocity = Vector2();
	animationController = NULL;
	staticSprite = NULL;
	triggerObject = NULL;
	Active = true;
}

BoundingBox Entity::GetBoundingBox(Vector2 topLeftPosition)
{
	BoundingBox box = BoundingBox();
	box.position = topLeftPosition;
	box.position.x += (g_game->TILE_SIZE * g_game->Scale) / 2.0f; //position at centre of tile
	box.position.y += (g_game->TILE_SIZE * g_game->Scale) / 2.0f; //position at centre of tile
	box.Width = (float)(width * g_game->Scale);
	box.Height = (float)(height * g_game->Scale);
	if (triggerObject != NULL)
		box.Trigger = true;
	return box;
}
void Entity::Update()
{
	if (animationController != NULL)
		animationController->Update();

	if (velocity.ApproxZero())
		return;

	Vector2 nextPosition = position.Add(velocity.Multiply(g_CurrentTime.DeltaTime * g_game->Scale));
	if (g_game->physics->AllowedMovement( nextPosition,this))
	{
		position.x = nextPosition.x;
		position.y = nextPosition.y;
		g_game->physics->RunTriggerCollision(this);

	}



	/*nextPosition.x =  entity->position.x;
	nextPosition.y = entity->position.y;
	Vector2 vecMov = Vector2::VectorForDirection(dirn);
	vecMov.Multiply((g_game->Scale * entity->width));
	nextPosition = nextPosition.Add(vecMov);
	entity->SetPosition(nextPosition.x, nextPosition.y);*/
}

void Entity::SetPosition(float x, float y)
{
	position.x = x;
	position.y = y;
}

void Entity::SetPosition(Vector2 positionVector)
{
	position.x = positionVector.x;
	position.y = positionVector.y;
}

void Entity::Render()
{
	if (!Active)
		return;
	SDL_Rect destRect;
	Vector2 renderPos = g_game->WorldPositionToRenderPosition(position.x, position.y);
	destRect.x = MathUtility::RoundFloat(renderPos.x);
	destRect.y = MathUtility::RoundFloat(renderPos.y);
	destRect.w = width * g_game->Scale;
	destRect.h = height * g_game->Scale;

	if (animationController != NULL)
	{
		Sprite animationSprite = animationController->GetCurrentSprite();
		SDL_BlitSurface(animationSprite.SourceSurface, animationSprite.SourceRect, g_game->screenSurface, &destRect);
	}
	else
	{
		SDL_BlitSurface(staticSprite->SourceSurface, staticSprite->SourceRect, g_game->screenSurface, &destRect);
	}
}

void Entity::SetSprite(SDL_Surface* sdl_sprite, SDL_Rect* sourceRect)
{
	staticSprite = new Sprite();
	staticSprite->SourceSurface = sdl_sprite;
	staticSprite->SourceRect = sourceRect;
}

void Entity::SetSprite(Sprite sprite)
{
	staticSprite = new Sprite();
	staticSprite->SourceSurface = sprite.SourceSurface;
	staticSprite->SourceRect = sprite.SourceRect;
}

Entity::~Entity()
{
	if (staticSprite != NULL)
	{
		delete(staticSprite);
		staticSprite = NULL;
	}


}
