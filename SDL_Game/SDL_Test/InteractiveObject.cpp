#include "InteractiveObject.h"



InteractiveObject::InteractiveObject(IObjType objType, int x, int y)
{
	this->objType = objType;
	this->entity = new Entity();
	entity->entityType = EntityObject;
	entity->SetPosition(g_game->CoordinateToWorldPosition(x, y));
	if (objType == IObjType::Chest)
		entity->SetSprite(g_game->resourceManager->ChestSurface, &(this->sourceRectClosedChest));
	dialogObject = NULL;
	enemyInfo = NULL;
	chestInfo = {};
}

void InteractiveObject::InteractStart()
{
	char messageBuf[STR_BUF_SIZE];
	DialogObject* dialogObject = NULL;
	if (objType == IObjType::Chest)
	{
		if (chestInfo.opened)
			return;
		dialogObject = new DialogObject(this);
		entity->SetSprite(g_game->resourceManager->ChestSurface, &(this->sourceRectOpenChest));
		snprintf(messageBuf, STR_BUF_SIZE, "You found a chest\ncontaining:");
		AppendStringForItem(messageBuf, chestInfo.itemSlot1);
		AppendStringForItem(messageBuf, chestInfo.itemSlot2);
		AppendStringForItem(messageBuf, chestInfo.itemSlot3);
		strcpy(dialogObject->message, messageBuf);
		g_game->AddChestObjectsToInventory(&chestInfo);
	}
	if (objType == IObjType::Enemy)
	{
		if (enemyInfo->Finished)
			return;
		dialogObject = new DialogObject(this);
		strcpy(dialogObject->message,"You Won't Make it any\nfurther.\nPrepare To Die");
	}
	g_game->OpenDialogue(dialogObject);
}

void InteractiveObject::AppendStringForItem(char* messageBuf, ItemType itemType)
{
	if (itemType != ItemType::NullItem)
	{
		strcat(messageBuf, "\n");
		strcat(messageBuf, FindItemForType(itemType)->ItemName);
	}
}
void InteractiveObject::InteractEnd()
{
	if (dialogObject != NULL)
		delete(dialogObject);
	if (objType == IObjType::Enemy)
	{
		g_game->StartBattle(enemyInfo);
		enemyInfo->Finished = true;
		entity->Active = false;
	

	}
}
InteractiveObject::~InteractiveObject()
{
}
