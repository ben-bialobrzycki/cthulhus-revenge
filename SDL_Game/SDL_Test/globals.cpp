#include "globals.h"
#include "Game.h"
#include "GameTime.h"
#include "Item.h"
#include "MagicSpell.h"
#include "InteractiveObject.h"

Game* g_game;
GameTime g_CurrentTime;
FrameInput g_Input;
FrameInput g_PrevInput;

Item AllItems[] = {
	{ "Potion", ItemType::HealPotion,false,false,30,0 },
	{ "Elixir", ItemType::MPPotion,false,false,20,0 },
	{"", ItemType::NullItem}
};

MagicSpell AllSpells[] = {
	{ "Ink",Spell::Ink,5,true,false,true,10,20},
	{ "",Spell::NullSpell,0 }

};

EnemyInfo AllEnemies[] = {
	{"Pirate",EnemyType::Pirate,3,5,15,3,0, SpriteId::BattlePirate},
	{ "Pirate King",EnemyType::BossPirate,6,10,35,6,0,SpriteId::BattlePirateKing },
	{ "Pirate Dog",EnemyType::PirateDog,2,5,10,6,0,SpriteId::BattleDog },
	{ "",EnemyType::None,0,0,0,0,0,SpriteId::None }
};
