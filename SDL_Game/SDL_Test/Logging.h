#pragma once
#include <string>

class Logging
{
public:
	Logging();
	void LogMessage(std::string message);
	~Logging();
};

