#include "TriggerObject.h"
#include "Common.h"
#include "globals.h"
#include "Game.h"
#include "Entity.h"


TriggerObject::TriggerObject(TrigObjType objType, int x, int y)
{
	this->objType = objType;
	entity = new Entity();
	entity->triggerObject = this;
	entity->entityType = EntityObject;
	entity->SetPosition(g_game->CoordinateToWorldPosition(x, y));
	
	switch (objType)
	{
	case TrigObjType::StairsDown: entity->SetSprite(g_game->resourceManager->GetSpriteForId(SpriteId::StairsDown)); 
		entity->width = entity->height = StairSize; break;

	case TrigObjType::StairsUp: entity->SetSprite(g_game->resourceManager->GetSpriteForId(SpriteId::StairsUp)); 
		entity->width = entity->height = StairSize; break;
		break;

	}
}


void TriggerObject::GoDownStairs(Entity* playerEntity)
{
	g_game->ChangeLevel(-1,this->entity->position);
}

void TriggerObject::GoUpStairs(Entity* playerEntity)
{
	g_game->ChangeLevel(1, this->entity->position);
}
void TriggerObject::OnCollision(Entity * otherEntity)
{
	switch (objType)
	{
	case TrigObjType::StairsDown: GoDownStairs(otherEntity); break;
	case TrigObjType::StairsUp: GoUpStairs(otherEntity); break;
	default:
		break;
	}
}
TriggerObject::~TriggerObject()
{
}
