#include "Timer.h"



Timer::Timer()
{
	OnDone = NULL;
	Finished = false;
	On = false;
}

void Timer::Start(float time, std::function<void()> fnOnDone)
{
	timeInitial = time;
	timeLeft = timeInitial;
	OnDone = fnOnDone;
	Finished = false;
	On = true;
}

void Timer::Update()
{
	if (Finished || !On)
		return;

	timeLeft -= g_CurrentTime.DeltaTime;
	if (timeLeft < 0)
	{
		Finished = true;
		On = false;
		if (OnDone != NULL) {
			OnDone();
		}
	}

}



Timer::~Timer()
{
}
