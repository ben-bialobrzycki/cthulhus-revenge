#pragma once
#include "Timer.h"

class Transition
{
public:
	Transition();

	void Initialise();
	void Update();
	void Render();
	bool Finished();
	~Transition();
private:
	Timer timer;
	void RenderFade(float perc);
	void RenderWave(float perc);
	SDL_Texture* backgroundTexture;
	SDL_Surface* originalSurface;
	int framCount;
};

