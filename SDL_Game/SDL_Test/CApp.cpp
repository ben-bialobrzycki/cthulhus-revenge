
//Using SDL and standard IO
#include <SDL.h>
#include <stdio.h>
#include "Game.h"
#include <vector>
//#include "globals.h"

extern Game* g_game;

int main(int argc, char* args[])
{
	SDL_LogSetAllPriority(SDL_LOG_PRIORITY_WARN);
	SDL_Log("Starting Application");
	printf("Starting Application");
	SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION,"Debug Starting Application");
	g_game = new Game();
	if (!g_game->Initialise())
		return 0;

	g_game->Load();

	g_game->RunLoop();
	g_game->Close();

	
	return 0;
}