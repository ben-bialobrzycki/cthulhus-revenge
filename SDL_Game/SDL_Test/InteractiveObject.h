#pragma once
#include "globals.h"
#include "Common.h"
#include "Game.h"
#include "DialogObject.h"
#include "globals.h"
#include "InteractiveObjectTypes.h"
enum class IObjType {Chest, Enemy};




class InteractiveObject
{
public:
	InteractiveObject(IObjType objType, int x, int y);
	~InteractiveObject();
	Entity* entity;
	IObjType objType;
	EnemyGroupInfo* enemyInfo;
	ChestInfo chestInfo;
	void InteractStart();
	void InteractEnd();

private:
	SDL_Rect sourceRectClosedChest = {0,0,16,16};
	SDL_Rect sourceRectOpenChest = {16,0,16,16};
	DialogObject* dialogObject;
	void AppendStringForItem(char* messageBuf, ItemType itemType);
};

