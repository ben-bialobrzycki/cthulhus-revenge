#include "TitleScreen.h"
#include "Common.h"
#include "globals.h"
#include "Game.h"
#include "SoundSystem.h"

TitleScreen::TitleScreen()
{
	Finished = false;
	animationController = AnimationController();
	Animation* introAnimation = g_game->resourceManager->GetAnimationForId(AnimationId::IntroAnimation);
	animationController.AddAnimation((int)AnimationId::IntroAnimation, introAnimation);
	animationController.PlayAnimation((int)AnimationId::IntroAnimation);
}

void TitleScreen::Run()
{
	g_game->soundSystem->PlayMusic(Music::TitleScreen);

	animationController.Update();

	Sprite sprite = animationController.GetCurrentSprite();
	SDL_BlitSurface(sprite.SourceSurface, sprite.SourceRect, g_game->screenSurface, NULL);

	if (g_Input.APressed)
	{
		Finished = true;
		g_game->soundSystem->StopMusic();
		g_game->InitialiseMainGame();
		g_game->SetNewPlayState(Normal);
	}
}


TitleScreen::~TitleScreen()
{
}
