#pragma once
#include "Location.h"
#include "Common.h"

class WorldMap
{
public:
	WorldMap(int height, int width);
	~WorldMap();
	int Height, Width;
	Location* LocationAt(int x, int y);

private:
	Location*** locations; //2d array of location* ;
};

