#pragma once
#include "Common.h"
class Animation
{
public:
	//Animation();
	Animation(SDL_Surface* sheet, int frameWidth, int frameHeight, std::initializer_list<int> frame_indicies, bool loop = true);
	~Animation();
	std::vector<SDL_Rect*>* frames;
	SDL_Surface* spriteSheet;
	void SetFrames(SDL_Surface* sheet, int frameWidth, int frameHeight, std::initializer_list<int> frames);
	float TimePerFrame = 0.2f;
	bool Loop;
};

