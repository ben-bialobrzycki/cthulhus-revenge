#pragma once
#include "Common.h"
//#include <SDL.h>
#include <stdio.h>
#include <vector>
#include "Wall.h"
#include "WorldMap.h"
#include "LevelMaps.h"
#include "TriggerObject.h"
#include "defs.h"
#include "MathUtility.h"


struct Level
{
	WorldMap* worldMap = NULL;
	std::vector<std::vector<char>*>* tiles = NULL;
	std::vector<pEntity> entities;
};
class LevelManager
{
public:
	LevelManager();
	void Initialise();
	void Render();
	void SetTileMap(SDL_Surface* sdl_sprite);
	const int height = 28;
	const int width = 20;

	int TILE_SIZE = 16;
	SDL_Rect DestRectForCoordinates(int x, int y);
	void RenderTileChar(int x, int y, char tilechar);
	SDL_Rect SourceRectForChar(char c);
	std::vector<Wall*> walls;
	//WorldMap* currentWorldMap;
	Level* currentLevel;
	
	bool ChangeLevel(UINT levelDiff);
	

	~LevelManager();

private:
	int levelIndex = 0;
	std::vector<Level> levels;
	SDL_Surface* tilemapsurface;
	void SetLevelObjects(Level* level, EnemyGroupInfo* levelEnemies, ChestInfo* levelChests);
	WallDirection IsWall(char c);
	InteractiveObject* InteractiveObjectForChar(char c, int x, int y);
	TriggerObject* TriggerObjectForChar(char c, int x, int y);
	void AddLevel(char tilemap[LEVEL1_HEIGHT][LEVEL1_WIDTH], int height, int width, EnemyGroupInfo* levelEnemies, ChestInfo* levelChests);
};

