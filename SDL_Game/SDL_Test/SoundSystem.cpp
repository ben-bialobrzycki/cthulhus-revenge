#include "SoundSystem.h"
#include "globals.h"
#include "Game.h"
#include "ResourceManager.h"

SoundSystem::SoundSystem()
{
}

#ifdef __EMSCRIPTEN__
//NO SDL_Mixer support
void SoundSystem::PlayMusic(Music music)
{
}
void SoundSystem::StopMusic()
{
	currentMusic = Music::None;
}
bool SoundSystem::Initialise()
{
	return true;
}

#else
void SoundSystem::PlayMusic(Music music)
{
	Mix_Music* musicAudio = NULL;
	if (currentMusic != music)// Mix_PlayingMusic() == 0)
	{
		currentMusic = music;
		//Play the music

		switch (currentMusic)
		{
		case Music::TitleScreen: musicAudio = titleMusic; break;
		case Music::Battle: musicAudio = battleMusic; break;
		default:
			break;
		}
		if (musicAudio != NULL)
			Mix_PlayMusic(musicAudio, -1);

	}
}


void SoundSystem::StopMusic()
{
	currentMusic = Music::None;
	Mix_FadeOutMusic(500);
}

bool SoundSystem::Initialise()
{
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		return false;
	}
	bool failed = false;
	titleMusic = TryLoadMusicFile("assets/sounds/cthulutitle.mp3", &failed);
	battleMusic = TryLoadMusicFile("assets/sounds/cthulubattle.mp3", &failed);
	return failed;
}

Mix_Music* SoundSystem::TryLoadMusicFile(char* fileName, bool* failFlag)
{
	
	Mix_Music* titleMusic = Mix_LoadMUS(fileName);
	if (titleMusic == NULL)
	{
		printf("Failed to load music file %s! SDL_mixer Error: %s\n",fileName, Mix_GetError());
		*failFlag = true;
		return NULL;
	}
	return titleMusic;
}
SoundSystem::~SoundSystem()
{
	//Free the music
	Mix_FreeMusic(titleMusic);
	titleMusic = NULL;
}
#endif