#include "Animation.h"

Animation::Animation(SDL_Surface* sheet, int frameWidth, int frameHeight, std::initializer_list<int> frame_indicies, bool loop)
{
	SetFrames(sheet, frameWidth, frameHeight, frame_indicies);
	Loop = loop;

}
//assumes horizontal strips
void Animation::SetFrames(SDL_Surface* sheet, int frameWidth, int frameHeight, std::initializer_list<int> frame_indicies)
{
	frames = new std::vector<SDL_Rect*>();
	spriteSheet = sheet;
	for (auto frame : frame_indicies)
	{
		SDL_Rect* rect = new SDL_Rect();
		rect->x = frameWidth * frame;
		rect->w = frameWidth;
		rect->h = frameHeight;
		rect->y = 0;
		frames->push_back(rect);
	}
}
Animation::~Animation()
{
	delete(frames);
}
