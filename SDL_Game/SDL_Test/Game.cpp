#include "Game.h"
#include "Player.h"
#include "Enemy.h"
#include "Direction.h"
#include "LevelManager.h"
#include "GameTime.h"
#include "SpriteSheet.h"
#include "Battle.h"
#include "PlayerParty.h"
#include "GameOverState.h"
#include "InventoryItem.h"
#include "Transition.h"
#include "TitleScreen.h"
#include "WinState.h"
#include "SoundSystem.h"

bool Game::Initialise()
{
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return false;
	}
	
		int imgFlags = IMG_INIT_PNG;
		if (!(IMG_Init(imgFlags) & imgFlags))
		{
			printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
			return false;
		}

		//SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT,0,&window,&renderer);
		window = SDL_CreateWindow("Cthulus Revenge", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			return false;
		}
		
			windowscreenSurface = SDL_GetWindowSurface(window);
		
		screenSurface = SDL_CreateRGBSurface(0, GAME_SCREENWIDTH, GAME_SCREENHEIGHT, 32,RED_MASK, GREEN_MASK,BLUE_MASK,ALPHA_MASK);
		if (screenSurface == NULL)
		{
			printf("Could not create screen surface %s", SDL_GetError());
			return false;
		}
		//TODO use hardware accelerated rendering
		//renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		renderer = SDL_CreateSoftwareRenderer(screenSurface);
		if (renderer == NULL)
		{
			printf("Could not create renderer %s",SDL_GetError());
			return false;
		}

		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

		if (TTF_Init() == -1)
		{
			printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
			return false;
		}
	
		soundSystem = new SoundSystem();
		success = soundSystem->Initialise();
	return true;
}



void Game::RunLoop()
{
	quit = false;

	while (!quit)
	{
		g_CurrentTime.Update();
		GetInput();
		//DEBUG
		//if (g_Input.BPressed)
		//{
		//	DoDebugThing();
		//}
		//DEBUG
		if (playState == PlayStateBattle)
		{
			currentBattle->Update();
			if (currentBattle->Finished())
				FinishBattle();
		}
		else if (playState == BattleIntro)
		{
			battleIntro->Update();
			if (battleIntro->Finished()) 
			{
				if (currentBattle != NULL)
					playState = PlayStateBattle;
				else
					playState = Normal;
				delete(battleIntro);
			}
			
		}
		else if (playState == Normal || playState == Dialogue)
		{
			UpdateMain();
		}
		else if (playState == ItemMenu)
		{
			HandleItemMenu();
		}
		else if (playState == GameOver)
		{
			if (g_Input.APressed)
			{
				titleScreenState->Finished = false;
				playState = IntroScreen;
			}
			gameOverState->Update();
			//gameOverState->Render();
		}
		else if (playState == IntroScreen)
		{
			StartRender();
			titleScreenState->Run();
			FinishRender();
			continue;
		}
		else if (playState == GameComplete)
		{
			StartRender();
			winScreenState->Run();
			FinishRender();
			continue;
		}
		Render();
	}
}


void Game::HandleItemMenu()
{
	dialogueManager->Update();
	//TODO tidy up copy paste Dialogue
	DialogueInputResult dialogueResult = dialogueManager->HandleInput();

	switch (dialogueResult.command)
	{
		case MagicChoose: dialogueManager->SetCurrentMenu(MagicChoice, playerParty, &(playerParty->playerCharacter->MagicOptions)); break;
		case ItemChoose: dialogueManager->SetCurrentMenu(ItemChoice, playerParty, &(playerParty->ItemOptions)); break;
		case DialogueUseItem:
		{
			 
			Item* item = FindItemForType((ItemType)dialogueResult.commandParameter);
			UseItem(item, playerParty, false);
		}break;
		case QuitDialogue: 
		{
			if (dialogueResult.selectedMenu != FightChoice)
				dialogueManager->SetCurrentMenu(FightChoice, playerParty, mainMenuOptions);
			else
				SetNewPlayState(Normal); 
			
		}break;
		
		default:
			break;
	}
	
}

void Game::UpdateMain()
{
	if (!doneIntro)
	{
		dialogueManager->SetTextForSlowMessage(resourceManager->IntroText);
		playState = Dialogue;
		doneIntro = true;
	}

	if (playState == Normal)
	{
		if (g_Input.APressed)
		{
			Location* playerLocation = LocationForWorldPosition(player->entity->position);
			int x = 0; int y = 0;
			switch (player->currentDirection)
			{
				case Up: y = -1; break;
				case Down: y = 1; break;
				case Left: x = -1; break;
				case Right: x = 1; break;
			}
			Location* facingLocation = levelManager->currentLevel->worldMap->LocationAt(playerLocation->x + x, playerLocation->y + y);
			if (facingLocation == NULL)
				return;
			if (facingLocation->locationObject != NULL)
			{
				facingLocation->locationObject->InteractStart();
			}
		}
		if (g_Input.BPressed)
		{
			SetNewPlayState(ItemMenu);
		}

		if (g_Input.UpIsDown)
			player->SetInput(Up);
		else if (g_Input.DownIsDown)
			player->SetInput(Down);
		else if (g_Input.LeftIsDown)
			player->SetInput(Left);
		else if (g_Input.RightIsDown)
			player->SetInput(Right);
		else 
			player->SetInput(None);
	}

	if (playState == Dialogue)
	{
		if (g_Input.APressed && dialogueManager->finishedDisplayText)
		{
			CloseDialogue();
		}
		dialogueManager->Update();
	}
	
	
	//Update
	std::for_each(entities.begin(), entities.end(), [](pEntity &e) {e->Update(); });
}


void Game::SetNewPlayState(PlayState nextState)
{
	if (nextState == ItemMenu)
		dialogueManager->SetCurrentMenu(FightChoice, playerParty, mainMenuOptions);
	playState = nextState;
}

void Game::StartBattle(EnemyGroupInfo* enemyGroupInfo)
{
	currentBattle = new Battle(dialogueManager, playerParty, enemyGroupInfo);
	currentBattle->Initialise();
	battleIntro = new Transition();
	battleIntro->Initialise();
	playState = BattleIntro;
}

void Game::HandleGameEvent(GameEventId gameEvent)
{
	if (gameEvent == GameEventId::GameComplete)
	{
		winScreenState->Initialise();
		SetNewPlayState(GameComplete);

	}

}
void Game::FinishBattle()
{
	GameEventId gameEvent = currentBattle->enemyGroupInfo->gameEvent;
	delete(currentBattle);
	currentBattle = NULL;
	if (playerParty->playerCharacter->HitPoints <= 0)
	{
		playState = GameOver;
		gameOverState = new GameOverState(dialogueManager);
		return;
	}
	else
	{
		playState = Normal;
	}
	HandleGameEvent(gameEvent);

	
}

void Game::StartRender()
{
	
	SDL_BlitSurface(resourceManager->backgroundSurface, NULL, screenSurface, NULL);
}

void Game::FinishRender()
{
	//Update the surface
	SDL_BlitScaled(screenSurface, &sourceGameRect, windowscreenSurface, &screenGameRect);
	SDL_UpdateWindowSurface(window);
}
void Game::Render()
{
	StartRender();
	
	if (playState == PlayStateBattle)
	{
		SDL_SetRenderDrawColor(renderer, GREEN0);
		SDL_Rect rect = { 0,0,GAME_SCREENWIDTH,GAME_SCREENHEIGHT };
		SDL_RenderDrawRect(renderer, &rect);
		currentBattle->Render();
	}
	if (playState == Dialogue || playState == Normal || playState == ItemMenu)
	{
		levelManager->Render();
		std::for_each (entities.begin(), entities.end(), [](pEntity &e) {e->Render(); });
		DrawUIOverlay();
		//DEBUG Position
		//SDL_Rect playerPosRect = { 5,5,40,20 };
		//dialogueManager->RenderTextLine(player->entity->position.ToString().c_str(), { 0xFF,0xFF,0xFF,0xFF }, playerPosRect);
	}
	
	if (playState == BattleIntro)
	{
		battleIntro->Render();
	}
	if (playState == GameOver)
	{
		gameOverState->Render();
	}
	
	FinishRender();
}

Vector2 Game::CoordinateToWorldPosition(int x, int y)
{
	return  Vector2((float)(x * levelManager->TILE_SIZE * Scale),(float)(y * levelManager->TILE_SIZE * Scale));
}

Location* Game::LocationForWorldPosition(Vector2 position)
{
	int x = ((int)position.x) / (levelManager->TILE_SIZE * Scale);
	int y = ((int)position.y) / (levelManager->TILE_SIZE * Scale);
	return levelManager->currentLevel->worldMap->LocationAt(x, y);
}

Vector2 Game::WorldPositionToRenderPosition(float  x, float y)
{
	float centrex = player->entity->position.x;
	float centrey = player->entity->position.y;
	Vector2 renderPos = Vector2();
	Vector2 centrePosition = CoordinateToWorldPosition(5, 4);
	renderPos.x = centrePosition.x - (centrex - x);
	renderPos.y = centrePosition.y - (centrey - y);
	return renderPos;
}

void Game::GetInput()
{
	SetInputForFrame();
}

void Game::SetInputForFrame()
{
	g_PrevInput = g_Input;
	g_Input = {};
	previousKeyboardState = *keyboardState;
	SDL_Event e;
	while (SDL_PollEvent(&e) != 0) /*this calls PumpEvents and updates keyboardState*/
	{
		if (e.type == SDL_QUIT)
			quit = true;
	}

	if (keyboardState[SDL_SCANCODE_UP])
		g_Input.UpIsDown = true;
	else if (keyboardState[SDL_SCANCODE_DOWN])
		g_Input.DownIsDown = true;
	else if (keyboardState[SDL_SCANCODE_LEFT])
		g_Input.LeftIsDown = true;
	else if (keyboardState[SDL_SCANCODE_RIGHT])
		g_Input.RightIsDown = true;

	if (keyboardState[SDL_SCANCODE_X])
		g_Input.AIsDown = true;
	if (keyboardState[SDL_SCANCODE_Z])
		g_Input.BIsDown = true;

	if (!g_PrevInput.AIsDown && g_Input.AIsDown)
		g_Input.APressed = true;
	if (!g_PrevInput.BIsDown && g_Input.BIsDown)
		g_Input.BPressed = true;
	if (!g_PrevInput.AIsDown && g_Input.AIsDown)
		g_Input.APressed = true;

	if (!g_PrevInput.LeftIsDown && g_Input.LeftIsDown)
		g_Input.LeftPressed = true;
	if (!g_PrevInput.RightIsDown && g_Input.RightIsDown)
		g_Input.RightPressed = true;
	if (!g_PrevInput.UpIsDown && g_Input.UpIsDown)
		g_Input.UpPressed = true;
	if (!g_PrevInput.DownIsDown && g_Input.DownIsDown)
		g_Input.DownPressed = true;

}


void Game::OpenDialogue(DialogObject* dialogObject)
{
	currentDialogObject = dialogObject;
	playState = Dialogue;
	dialogueManager->SetTextForSlowMessage(currentDialogObject->message);
}
void Game::CloseDialogue()
{
	playState = Normal;
	if (currentDialogObject != NULL)
	{
		currentDialogObject->initiatingObject->InteractEnd();
	}
	
}

bool Game::Load()
{
	resourceManager = new ResourceManager();
	dialogueManager = new DialogueManager();
	dialogueManager->Initialise();
	bool success = resourceManager->LoadResources();
	keyboardState = SDL_GetKeyboardState(NULL);
	InitialiseGameObjects();
	return success;
}

void Game::ChangeLevel(int levelDiff, Vector2 position)
{
	printf("Called Change Level %d", levelDiff);
	if (levelManager->ChangeLevel(levelDiff))
		SetForLevel(position);
}

int Game::GetRandomInt(int min,int max)
{
	int cap = max - min;
	UINT result = RandomNumber();
	int clamped = result % cap;
	return (int)clamped + min;
}

void Game::AddItemToInventory(ItemType item) {

	for (UINT i = 0; i < playerParty->inventory->size(); i++) {
		if (playerParty->inventory->at(i).itemType == item) 
		{
			playerParty->inventory->at(i).Quanity++;
			return;
		}
	}
	InventoryItem invItem = InventoryItem();
	invItem.itemType = item;
	invItem.Quanity = 1;
	playerParty->inventory->push_back(invItem);
}

void Game::AddChestObjectsToInventory(ChestInfo* chestContents)
{
	AddItemToInventory(chestContents->itemSlot1);
	AddItemToInventory(chestContents->itemSlot2);
	AddItemToInventory(chestContents->itemSlot3);
	chestContents->itemSlot1 = chestContents->itemSlot2 = chestContents->itemSlot3 = ItemType::NullItem;
	chestContents->opened = true;
	playerParty->RefreshItemInfo();
}

void Game::InitialiseGameObjects()
{
	sourceGameRect = SDL_Rect();
	sourceGameRect.x = 0; sourceGameRect.y = 0;
	sourceGameRect.w = GAME_SCREENWIDTH; sourceGameRect.h = GAME_SCREENHEIGHT;

	screenGameRect = SDL_Rect();
	screenGameRect.x = 0; screenGameRect.y = 0;
	screenGameRect.w = SCREEN_WIDTH; screenGameRect.h = SCREEN_HEIGHT;

	randomSeed = (unsigned)time(0);
	RandomNumber = std::default_random_engine();
	RandomNumber.seed(randomSeed);
	
	srand(randomSeed);

	titleScreenState = new TitleScreen();
	winScreenState = new WinState();

	
	playState = IntroScreen;

	mainMenuOptions = new std::vector<DialogueOption>();
	mainMenuOptions->push_back({ "Magic",0, MagicChoose,0 });
	mainMenuOptions->push_back({ "Item",0, ItemChoose,0 });

	InitialiseMainGame();
}

void Game::InitialiseMainGame()
{
	CleanupPreviousGame();
	currentBattle = NULL;
	entities = std::vector<pEntity>();
	enemies = std::vector<Enemy*>();
	levelManager = new LevelManager();
	levelManager->SetTileMap(resourceManager->FloorTileMapSurface);
	levelManager->Initialise();
	physics = new Physics(&entities);
	player = new Player();
	playerParty = new PlayerParty();
	doneIntro = false;
	currentDialogObject = NULL;
	SetForLevel(CoordinateToWorldPosition(10, 10));
}

//DEBUG
void Game::DoDebugThing()
{
	battleIntro = new Transition();
	battleIntro->Initialise();
	playState = BattleIntro;
}
void Game::CleanupPreviousGame()
{
	if (levelManager)
		delete(levelManager);
	if (physics)
		delete(physics);
	if (player)
		delete(player);
	if (playerParty)
		delete(playerParty);
}

void Game::SetForLevel(Vector2 position)
{
	entities.clear();
	Location* startLoc = LocationForWorldPosition(position);
	Vector2 world = CoordinateToWorldPosition(startLoc->x, startLoc->y + 1); //Move Off Stairs by 1 tile to avoid Retrigger;
	player->entity->SetPosition(world);
	for (UINT i = 0; i < levelManager->currentLevel->entities.size(); i++) {
		entities.push_back(levelManager->currentLevel->entities[i]);
	}

	physics->SetWorldMap(levelManager->currentLevel->worldMap);
	entities.push_back(player->entity);
}

void Game::DrawUIOverlay()
{
	if (playState == Normal)
		return;
	if (playState == Dialogue)
	{
		std::string msg;
		if (currentDialogObject != NULL)
			msg = currentDialogObject->message;
		else
			msg = "Nothing to see here";

		dialogueManager->Render();
	}
	if (playState == ItemMenu)
	{
		dialogueManager->RenderPlayerUI(playerParty);
	}
}

//debug function
void Game::DrawBoundingBoxes()
{
	SDL_SetRenderDrawColor(renderer, GREEN0);
	SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0x00, 0xFF);
	for (size_t i = 0; i < entities.size(); i++)
	{
		Entity* entitiy = entities.at(i);
		if (entitiy->entityType == EntityWall || entitiy->entityType == EntityPlayer)
		{
			BoundingBox box = entitiy->GetBoundingBox(entitiy->position);
			SDL_Rect rect = SDL_Rect();
			rect.x = (int)(box.position.x - box.Width / 2.0f);
			rect.y = (int)(box.position.y - box.Height / 2.0f);
			rect.w = (int)box.Width;
			rect.h = (int)box.Height;
			SDL_RenderFillRect(renderer, &rect);
		}
	}
	SDL_RenderPresent(renderer);
}

void Game::LogEntities()
{
	for (size_t i = 0; i < entities.size(); i++)
	{
		Entity* entitiy = entities.at(i);
		SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Entity Type %d Position", entitiy->entityType, entitiy->position.ToString().c_str());
		printf("\nEntity Type %d Position %s", entitiy->entityType, entitiy->position.ToString().c_str());
	}
}
void Game::Close()
{
	SDL_FreeSurface(resourceManager->backgroundSurface);
	SDL_DestroyWindow(window);
	window = NULL;
	SDL_Quit();
}

Game::Game()
{
	levelManager = NULL;
	physics = NULL;  
	player = NULL; 
	playerParty = NULL;
}


Game::~Game()
{
}

Item* FindItemForType(ItemType itemType)
{
	Item* item = NULL;
	for (int i = 0; AllItems[i].Type != ItemType::NullItem; i++)
	{
		if (itemType == AllItems[i].Type)
			item = &AllItems[i];
	}
	return item;
}

MagicSpell* FindSpell(Spell itemType)
{
	MagicSpell* item = NULL;
	for (int i = 0; AllSpells[i].Spell != Spell::NullSpell; i++)
	{
		if (itemType == AllSpells[i].Spell)
			item = &AllSpells[i];
	}
	return item;
}

bool UseItem(Item* item, PlayerParty* playerParty, bool inCombat)
{

	if (inCombat && item->CombatOnly)
		return false;
	if (!inCombat && item->NonCombatOnly)
		return false;
	
	playerParty->RemoveInventoryItem(item->Type);

	switch (item->Type)
	{
		case ItemType::HealPotion: playerParty->playerCharacter->Heal(item->Value1); break;
		case ItemType::MPPotion: playerParty->playerCharacter->AddMP(item->Value1); break;
	}
	return true;

}

bool UseSpell(MagicSpell* magicSpell, PlayerParty* playerParty, bool inCombat)
{
	//TODO use spells outside combat
	return false;
	if (inCombat && magicSpell->CombatOnly)
		return false;
	if (!inCombat && magicSpell->NonCombatOnly)
		return false;
}

void PlayErrorChoiceSound()
{
	//TODO add audio
}