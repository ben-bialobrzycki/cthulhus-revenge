#pragma once
#include "Vector2.h"
class BoundingBox
{
public:
	BoundingBox();
	Vector2 position;
	Vector2 OffSetPosition;
	float Width;
	float Height;
	bool Intersects(BoundingBox otherbox);
	bool Trigger;
	~BoundingBox();
};

