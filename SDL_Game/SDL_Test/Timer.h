#pragma once
#include "globals.h"
#include "GameTime.h"
class Timer
{
public:
	Timer();
	void Start(float time, std::function<void()> fnOnDone);
	void Update();
	bool Finished;
	bool On;
	std::function<void()>OnDone;
	float timeInitial;
	float timeLeft;

	~Timer();
private:
	
};

