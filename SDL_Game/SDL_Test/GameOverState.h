#pragma once
#include "AnimationController.h"
#include "DialogueManager.h"
class GameOverState
{
public:
	GameOverState(DialogueManager* dialogueManager);
	void Update();
	void Render();
	void HandleClick();
	~GameOverState();
private:
	AnimationController animationController;
	Animation* animation;
	SDL_Rect deathAnimationPosition = { 20,20,48,48 };
	SDL_Rect gameOverTextPostion = { 20,20 + 48,56,16 };
	DialogueManager* dialogueManager;
};

