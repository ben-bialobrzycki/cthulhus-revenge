#include "WorldMap.h"



WorldMap::WorldMap(int height, int width)
{
	Height = height;
	Width = width;
	this->locations = new Location**[height];
	for (int y = 0; y < height; y++)
	{
		this->locations[y] = new Location*[width];
		for (int x = 0; x < width; x++)
		{
			this->locations[y][x] = new Location(x,y);
		}
	}
}

Location* WorldMap::LocationAt(int x, int y)
{
	if (x < 0 || x >= Width)
		return NULL;
	if (y < 0 || y >= Height)
		return NULL;
	return this->locations[y][x];
}

WorldMap::~WorldMap()
{
}
