#ifdef __EMSCRIPTEN__
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_rect.h>
    #include <SDL2/SDL_surface.h>
    #include <SDL2/SDL_image.h>
    #include <SDL_ttf.h>
    #include <emscripten.h>
#endif

#ifdef  __linux__ 
    //linux code goes here
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_rect.h>
    #include <SDL2/SDL_surface.h>
    #include <SDL2/SDL_image.h>
    #include <SDL2/SDL_ttf.h>
    #include <SDL2/SDL_mixer.h>
#endif
#ifdef _WIN32
    // windows code goes here
    #pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

#endif


#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>
#include <memory>
#include <functional>
#include <algorithm>
#include "FrameInput.h"

//colours basic colours no ALPHA
#define HEXGREEN0 0x9BBC0FFF  //Light Green
#define HEXGREEN1 0x8BAC0FFF 
#define HEXGREEN2 0x306230FF 
#define HEXGREEN3 0x0F380FFF //Dark Green


#define GREEN0 0x9B, 0xBC, 0x0F, 0xFF  //Light Green
#define GREEN1 0x8B, 0xAC, 0x0F, 0xFF 
#define GREEN2 0x30, 0x62, 0x30, 0xFF 
#define GREEN3 0x0F, 0x38, 0x0F, 0xFF //Dark Green


