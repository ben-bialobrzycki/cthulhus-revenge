#pragma once
struct FrameInput
{
	bool APressed;
	bool BPressed;
	bool UpPressed;
	bool DownPressed;
	bool LeftPressed;
	bool RightPressed;

	bool AIsDown;
	bool BIsDown;
	bool UpIsDown;
	bool DownIsDown;
	bool LeftIsDown;
	bool RightIsDown;

};