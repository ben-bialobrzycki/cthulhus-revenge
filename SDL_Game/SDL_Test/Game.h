#pragma once

#include "Common.h"
#include <stdio.h>
#include <vector>
#include "Entity.h"

#include <ctime>
#include <random>
#include <string>
#include "Physics.h"

#include "ResourceManager.h"
#include "DialogueManager.h"
#include "InteractiveObjectTypes.h"
#include "BufDefs.h"

class Player;
class LevelManager;
class Location;
class DialogObject;
class Battle;
class pBattle;
class Transition;
class PlayerParty;
class Enemy;
class GameOverState;
class TitleScreen;
class WinState;
class SoundSystem;

enum PlayState {IntroScreen, Normal,Dialogue,ItemMenu,PlayStateBattle,BattleIntro, GameOver, GameComplete};
class Game
{
public:
	Game();
	bool Initialise();
	bool Load();
	void Close();
	void RunLoop();
	void UpdateMain();
	void LogEntities();
	void OpenDialogue(DialogObject* dialogObject);
	void CloseDialogue();
	
	void StartBattle(EnemyGroupInfo* enemyGroupInfo);
	void FinishBattle();
	std::vector<pEntity> entities;
	Physics* physics;

	int SCREEN_WIDTH = 640; //10
	int SCREEN_HEIGHT = 576; //9

	int GAME_SCREENWIDTH = 160;
	int GAME_SCREENHEIGHT = 144;
	Uint32 RED_MASK =   0xFF000000;
	Uint32 GREEN_MASK = 0x00FF0000;
	Uint32 BLUE_MASK =  0x0000FF00;
	Uint32 ALPHA_MASK = 0x000000FF;

	int Scale = 1;
	int TILE_SIZE = 16;

	SDL_Surface* screenSurface = NULL;
	SDL_Surface* windowscreenSurface = NULL;
	SDL_Surface* gamescreenSurface = NULL;
	Vector2 CoordinateToWorldPosition(int x, int y);
	Location* LocationForWorldPosition(Vector2 position);
	Vector2 WorldPositionToRenderPosition(float x, float y);
	ResourceManager* resourceManager;
	DialogueManager* dialogueManager;
	SoundSystem* soundSystem;
	Battle* currentBattle;
	GameOverState* gameOverState;
	bool quit;
	std::default_random_engine RandomNumber;
	void ChangeLevel(int levelDiff, Vector2 position);
	int GetRandomInt(int min, int max);
	void AddChestObjectsToInventory(ChestInfo* chestContents);
	const Uint8* keyboardState;
	Uint8 previousKeyboardState;
	SDL_Renderer* renderer;
	void SetNewPlayState(PlayState nextState);
	void InitialiseMainGame();
	void HandleGameEvent(GameEventId gameEvent);
private:
	void InitialiseGameObjects();
	void CleanupPreviousGame();
	void SetForLevel(Vector2 position);
	void Render();
	void DrawBoundingBoxes();
	void DrawUIOverlay();
	void AddItemToInventory(ItemType item);
	void StartRender();
	void FinishRender();
	SDL_Window* window = NULL;
	Player* player;
	std::vector<Enemy*> enemies;
	std::vector<DialogueOption>* mainMenuOptions;
	LevelManager* levelManager;
	PlayerParty* playerParty;
	void GetInput();
	void HandleItemMenu();
	void SetInputForFrame();
	
	Transition* battleIntro;
	
	PlayState playState;
	DialogObject* currentDialogObject;
	int randomSeed;
	TitleScreen* titleScreenState;
	WinState* winScreenState;
	SDL_Rect sourceGameRect;
	SDL_Rect screenGameRect;
	
	bool doneIntro;

	void DoDebugThing();
	~Game();
};

Item* FindItemForType(ItemType itemType);
bool UseItem(Item* itemType, PlayerParty* playerParty, bool inCombat);
MagicSpell* FindSpell(Spell itemType);
bool UseSpell(MagicSpell* spell, PlayerParty* playerParty, bool inCombat);
void PlayErrorChoiceSound();