#include "GameOverState.h"
#include "globals.h"
#include "Game.h"


GameOverState::GameOverState(DialogueManager* dialogueManager)
{
	this->dialogueManager = dialogueManager;
	animationController = AnimationController();
	Animation* deathAnim = g_game->resourceManager->GetAnimationForId(AnimationId::GameOverAnimation);
	deathAnim->Loop = false;
	animationController.AddAnimation((int)AnimationId::GameOverAnimation,deathAnim);
	animationController.PlayAnimation((int)AnimationId::GameOverAnimation);
	dialogueManager->SetTextForSlowMessage("Game Over");
}

void GameOverState::Update()
{
	animationController.Update();
	dialogueManager->Update();
}

void GameOverState::Render()
{
	Sprite sprite = animationController.GetCurrentSprite();
	SDL_BlitSurface(sprite.SourceSurface, sprite.SourceRect, g_game->screenSurface, &deathAnimationPosition);
	/*std::string gameOverMessage = "Game Over";
	SDL_Rect textRect = { g_game->GAME_SCREENWIDTH / 2, g_game->GAME_SCREENHEIGHT / 2,80,40 };
	dialogueManager->RenderTextLine(gameOverMessage, { 0xFF,0xFF,0xFF,0xFF }, textRect);
	dialogueManager->Render();*/
}

void GameOverState::HandleClick()
{
}


GameOverState::~GameOverState()
{
}
