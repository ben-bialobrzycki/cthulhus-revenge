#include "Common.h"
#include "globals.h"
#include "DialogueManager.h"
#include "Game.h"
#include <queue>
#include "BattleEnemy.h"
#include "PlayerParty.h"
#include "Timer.h"
#include "DialogueCommands.h"
#include "BufDefs.h"
#include "MathUtility.h"

#pragma once

enum class  BattleState { PlayerInput, PlayerTargetChoose, PlayerAttackPlay, EnemyInput, EnemyAttackPlay, PlayerDeath, PlayerVictory, PlayerMessageShow, EnemyMessageShow } ;

enum class CurrentTargetAction {AttackTarget,UseItemForTarget,UseSpellForTarget};
class Battle
{
public:
	Battle(DialogueManager* dialogueManager, PlayerParty* playerParty, EnemyGroupInfo* enemyGroupInfo);

	void SetEnemies();

	void Initialise();
	void SetDialogueInitial();
	void Update();
	void Render();
	
	bool Finished();
	EnemyGroupInfo* enemyGroupInfo;
	~Battle();
private:
	DialogueManager* dialogueManager;
	PlayerParty* playerParty;
	std::vector<BattleEnemy> enemies;
	BattleState state;
	void AddMessageToBuffer(char* message);
	AnimationController* animationController;
	void RenderEnemies();

	void RunPlayerAttack();
	void RunPlayerDefend();
	void FinishPlayerTurn();
	void RunEnemyTurn();
	void RunVictory();
	void RunPlayerDeath();
	void RunPlayerUseItem(ItemType item);
	void RunPlayerUseSpell(Spell spell);
	void SetOneEnemy(EnemyType enemyType);
	SDL_Rect CentreForSize(SDL_Rect target, int width, int height);
	bool isFinished;
	std::unique_ptr<Animation> playerAttack;
	std::unique_ptr<Timer> timer;
	SDL_Rect currentAnimationArea;
	SDL_Rect enemyRectPosition1 = { 8,8,28,56 };
	int BattleSpriteOffset = 32;
	void RenderCurrentAnimation();
	bool EnemiesLeft();
	void SetForPlayerInput();
	int selectedEnemyTarget;
	DialogueCommand currentCommand;
	void MoveTargetToNextPossible(bool right);
	
	
	void UseSpellForTarget();
	void UseAttackForTarget();
	void UseItemForTarget();
	MagicSpell* currentMagicSpell;
	Item* currentItem;
	CurrentTargetAction currentAction;
	BattleEnemy* GetSelectedEnemy();
	void PlayOverlayEnemyAnimation(AnimationId animation);

};


//typedef Battle* pBattle;

