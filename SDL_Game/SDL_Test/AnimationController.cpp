#include "AnimationController.h"
#include "globals.h"
#include "GameTime.h"

AnimationController::AnimationController()
{
	stopped = false;
}

Sprite AnimationController::GetCurrentSprite()
{

	if (currentAnimation == NULL)
	{
		Sprite nullSprite = Sprite();
		nullSprite.SourceRect = NULL;
		nullSprite.SourceSurface = NULL;
		return nullSprite;
	}
	
	UINT totalFrames = (UINT)(totalAnimationTime / currentAnimation->TimePerFrame);
	int frame = 0;
	if (!currentAnimation->Loop && totalFrames >= currentAnimation->frames->size())
		frame = currentAnimation->frames->size() -1;
	else
		frame = totalFrames % currentAnimation->frames->size();
		
	Sprite sprite = Sprite();
	sprite.SourceRect = currentAnimation->frames->at(frame);
	sprite.SourceSurface = currentAnimation->spriteSheet;
	return sprite;
	

}


void AnimationController::AddAnimation(int animationId, Animation* animation)
{
	animations[animationId] = animation;
}
void AnimationController::Update()
{
	if (stopped)
		return;
	totalAnimationTime += g_CurrentTime.DeltaTime;
}
void AnimationController:: PlayAnimation(int animationID)
{
	if (stopped)
		stopped = false;
	if (currentAnimationId == animationID) //already playing
		return;
	totalAnimationTime = 0.0f;
	currentAnimation = animations.at(animationID);
	currentAnimationId = animationID;
}

void AnimationController::Reset()
{
	totalAnimationTime = 0.0f;
}

void AnimationController::ClearCurrent()
{
	currentAnimation = NULL;
	stopped = true;
}

void AnimationController::StopAnimation()
{
	stopped = true;
}

AnimationController::~AnimationController()
{
	for (auto const &animationKey : animations) {
		delete(animationKey.second);
	}
}
