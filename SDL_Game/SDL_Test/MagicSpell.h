#pragma once
enum class Spell {Ink, Charm, NullSpell};
struct MagicSpell
{
	char* SpellName;
	Spell Spell;
	int MPCost;
	bool CombatOnly;
	bool NonCombatOnly;
	bool NeedTarget;
	int DamageMin;
	int DamageMax;

};

