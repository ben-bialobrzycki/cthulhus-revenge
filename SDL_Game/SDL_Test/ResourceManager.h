#pragma once
#include "Common.h"
#include <map>

enum class GameEventId { None,GameComplete};
enum class SpriteId { None, StairsUp, StairsDown, EnemyPirate,EnemyPirateKing, EnemyDog, BattlePirate, BattlePirateKing, BattleDog,  GameOverDeath };
enum class AnimationId {None,PlayerLeft, PlayerRight, PlayerUp, PlayerDown,  GameOverAnimation, BattlePlayAttack, BattleCursor, IntroAnimation, WinAnimation};
#include "Sprite.h"
#include "Animation.h"
const int SOURCE_TILE_SIZE = 16;
const int MAX_TILE_PER_SHEET = 10;

class ResourceManager
{
public:
	ResourceManager();
	~ResourceManager();
	bool LoadResources();
	SDL_Surface* backgroundSurface = NULL;
	SDL_Surface* pirateSurface = NULL;
	SDL_Surface* pirateKingSurface = NULL;
	SDL_Surface* pirateDogSurface = NULL;
	SDL_Surface* playerSurface = NULL;
	SDL_Surface* FloorTileMapSurface = NULL;
	SDL_Surface* DialogueBackgroundSurface = NULL;
	SDL_Surface* ChestSurface = NULL;
	SDL_Surface* PirateSurface = NULL;
	SDL_Surface* PirateKingBattleSurface = NULL;
	SDL_Surface* DogBattleSurface = NULL;
	
	SDL_Surface* BattleCursorSurface = NULL;

	SDL_Surface* BattlePlayerAttack = NULL;
	SDL_Surface* Cursor = NULL;
	SDL_Surface* FightDialogueBackgroundSurface = NULL;
	SDL_Surface* BattleBackgroundSurface = NULL;
	SDL_Surface* StairSurface = NULL;
	SDL_Surface* GameOverSurface = NULL;
	SDL_Surface* IntroImageSurface = NULL;
	SDL_Surface* WinImageSurface = NULL;
	Sprite GetSpriteForId(SpriteId spriteId);
	Animation* GetAnimationForId(AnimationId animationId);

	SDL_Rect sourceRects[MAX_TILE_PER_SHEET][MAX_TILE_PER_SHEET];

	char* OutroText;
	char* IntroText;



private:
	std::map<int, SDL_Surface*> spriteSheets;
	SDL_Surface* loadSurface(std::string path);
	void SetSoureRects();
	void SetTextBlocks();
	/*Sprite Sheets*/

};

