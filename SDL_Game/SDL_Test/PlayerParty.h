#pragma once
#include "PlayerCharacter.h"
#include "InventoryItem.h"
#include "defs.h"

class PlayerParty
{
public:
	PlayerParty();
	PlayerCharacter* playerCharacter;
	std::vector<InventoryItem>* inventory;
	std::vector<DialogueOption> ItemOptions;
	void RemoveInventoryItem(ItemType itemType);
	void RefreshItemInfo();
	~PlayerParty();
};

