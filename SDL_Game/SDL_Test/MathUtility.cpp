#include "MathUtility.h"



MathUtility::MathUtility()
{
}

int MathUtility::RoundFloat(float x)
{
	if (x < 0)
		return (int)(x - 0.5f);
	return (int)(x + 0.5f);

}

int MathUtility::ClampInt(int value, int min, int max)
{
	if (value < min)
		return min;
	if (value > max)
		return max;
	return value;
}

SDL_Rect MathUtility::CentreForSize(SDL_Rect target, int width, int height)
{
	int cx = target.x + target.w / 2;
	int cy = target.y + target.h / 2;
	SDL_Rect centred = { cx,cy,cx - (width / 2),cy - (height / 2) };
	return centred;
}

SDL_Rect MathUtility::TopLeftForCentre(SDL_Rect target)
{
	int cx = target.x - target.w / 2;
	int cy = target.y - target.h / 2;
	SDL_Rect centred = { cx,cy,target.w,target.h};
	return centred;
}

MathUtility::~MathUtility()
{
}
