#include "Common.h"
#include "DialogueCommands.h"
#include "Timer.h"
#include "PlayerParty.h"
#include "BufDefs.h"
#pragma once




class DialogueManager
{
public:
	DialogueManager();
	bool Initialise();
	void RenderText(char* msg, SDL_Color textColor, SDL_Rect rect,int lineSpacing);
	void RenderTextLine(const char* msg, SDL_Color textColor, SDL_Rect rect);
	void RenderCharTextLine(const char* msg, SDL_Color textColor, SDL_Rect rect);
	void Render();
	void RenderTop();
	void Update();
	void SetTextForSlowMessage(char* message);
	void AddTextForSlowMessage(char* message);
	bool finishedDisplayText;
	bool FastText;
	void RenderItemMenu(int cursorSelectedIndex);
	void RenderPlayerInfo(PlayerParty* playerParty);
	int messageBufferMax = 4;
	int cursorSelectedIndex = 0;
	~DialogueManager();
	void RenderPlayerUI(PlayerParty* playerParty);
	DialogueInputResult HandleInput();
	void SetCurrentMenu(MenuName currentMenu, PlayerParty* playerParty, std::vector<DialogueOption>* FightOptions);
	void RenderDialogueBackground();
private:
	SDL_Rect messageTextRectBottom = { 10,86,150,57 };
	SDL_Rect messageBorderRectBottom = { 0,82,160,62 };

	SDL_Rect messageTextRectTop = { 10,4,150,57 };
	SDL_Rect messageBorderRectTop = { 0,0,160,62 };

	char currentMessage[STR_MESSAGE_BUF_SIZE];
	MenuName currentMenu;
	int currentMessageIndex;
	TTF_Font *gFont = NULL;
	SDL_Surface* textSurface;
	SDL_Rect destRect;
	Timer messageTimer;
	float secondsPerCharNormal = 0.05f;
	float secondsPerCharFast = 0.025f;
	SDL_Color color = { GREEN3 };
	SDL_Color lightColor = { GREEN0 };
	int lineSpacing = 8;
	SDL_Rect borderRectRight = { 80,82,80,72 };
	SDL_Rect borderRectLeft = { 0,82,80,72 };
	SDL_Rect textRectRight ={ 106, 86, 75, 57 };
	SDL_Rect textRectPlayerInfo = { 10,86,75,57 };

	std::vector<DialogueOption>* currentMenuOptions;
	void DoNextMessageChar();

	char lineBuffer[STR_BUF_SIZE];
	std::vector<std::string> lines;
	char currentVisibleMessage[STR_MESSAGE_BUF_SIZE];
	char tempBuffer[STR_MESSAGE_BUF_SIZE];
	
};

