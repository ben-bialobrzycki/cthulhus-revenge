#pragma once
#include "Wall.h"
#include "InteractiveObject.h"
#include "TriggerObject.h"

class Location
{
public:
	Location(int x, int y);
	~Location();
	int x;
	int y;
	Wall* wall;
	InteractiveObject* locationObject;
	TriggerObject* triggerObject;
};

