#include "Transition.h"
#include "globals.h"
#include "Game.h"
#include "Common.h"
#include "math.h"

void Transition::Initialise()
{
#ifdef __EMSCRIPTEN__
	
#else
	backgroundTexture = SDL_CreateTextureFromSurface(g_game->renderer, g_game->screenSurface);
	originalSurface = SDL_CreateRGBSurface(0, g_game->GAME_SCREENWIDTH, g_game->GAME_SCREENHEIGHT, 32, g_game->RED_MASK, g_game->GREEN_MASK, g_game->BLUE_MASK, g_game->ALPHA_MASK);
	SDL_BlitSurface(g_game->screenSurface, NULL, originalSurface, NULL);
#endif
	timer = Timer();
	timer.Start(3.0f, NULL);
	framCount = 0;
}

void Transition::Render()
{
#ifdef __EMSCRIPTEN__
	
#else
	framCount++;
	SDL_SetRenderDrawColor(g_game->renderer, GREEN3);
	float perc = 1 - (((timer.timeLeft) / timer.timeInitial));
	SDL_RenderCopy(g_game->renderer, backgroundTexture, NULL, NULL);

	//RenderFade(perc);
	RenderWave(perc);
#endif
	//SDL_RenderPresent(g_game->renderer);
}


void Transition::RenderWave(float perc)
{
	Uint32 numPixels = originalSurface->w * originalSurface->h;
	
	Uint32* pixels = (Uint32*)originalSurface->pixels;
	Uint32* originalPixels = (Uint32*) pixels;
	
	float width = 50 * perc; //10;
	int bytesPerPixel = 4;
	Uint32* target_pixels = (Uint32*)g_game->screenSurface->pixels;
	int total_lines = originalSurface->h;
	
	for (Uint32 i = 0; i < numPixels; i++)
	{
		int line = i / (originalSurface->pitch / bytesPerPixel);
		int pix_on_line = i % (originalSurface->pitch / bytesPerPixel);

		Uint32 newColor;

		Uint32* pixel = pixels + i;
	
		float lineOffset = (float)line / total_lines;
		float phase = (perc * 4 * 2 * PI) + ((lineOffset) * 2 * PI);
		int offset = (int)(sinf(phase) * width);
		int newPos = offset + pix_on_line;

		if (newPos < 0 || newPos > ((originalSurface->pitch) / 4)) //pitch is in bytes it seems
			newColor = HEXGREEN3;
		else
			newColor = *pixel;
		
		if ( 0 <= i + offset && i + offset < numPixels)
			*(target_pixels + (i + offset)) = newColor;

	}
	
}
void Transition::RenderFade(float perc)
{
	Uint32 numPixels = g_game->screenSurface->w * g_game->screenSurface->h;
	Uint32* pixels = (Uint32*)g_game->screenSurface->pixels;
	Uint8 alpha = (Uint8)255 - (Uint8)(255 * perc);

	Uint32 colors[4] = { HEXGREEN0,HEXGREEN1,HEXGREEN2,HEXGREEN3 };

	int offset = MathUtility::RoundFloat( perc * 3);
	if (offset != 0)
	{
		for (Uint32 i = 0; i < numPixels; i++)
		{
			Uint32* pixel = pixels + i;

			for (int j = 0; j < 4; j++)
			{
				if (*pixel == colors[j] || *pixel == SDL_Swap32(colors[j]))
				{
					if (j + offset < 3)
						*pixel = colors[j + offset];
					else
						*pixel = colors[3];
					break;
				}
			}

		}
	}


}
void Transition::Update()
{
	timer.Update();
	//DEBUG
	//if (timer.Finished)
	//	printf("Number of frames was %d", framCount);
}
bool Transition::Finished()
{
	return timer.Finished;
}

Transition::Transition()
{
	
}


Transition::~Transition()
{
#ifdef __EMSCRIPTEN__
	
#else
	SDL_DestroyTexture(backgroundTexture);
	SDL_FreeSurface(originalSurface);
#endif
}
