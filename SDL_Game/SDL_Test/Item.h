#pragma once
enum class ItemType {HealPotion, MPPotion , NullItem};

struct Item {
	char* ItemName;
	ItemType Type;
	bool CombatOnly;
	bool NonCombatOnly;
	int Value1;
	int Value2;
};


